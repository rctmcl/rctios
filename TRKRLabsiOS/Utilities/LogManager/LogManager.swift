//
//  LogManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 10/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
class LogManager
{
    
    class var sharedInstance: LogManager {
        struct Static {
            static let instance: LogManager = LogManager()
        }
        return Static.instance
    }
    init()
    {
        
    }
    //MARK:  Log
    // Log Utility
    func DLog<T>(object: T, filename: String = __FILE__, line: Int = __LINE__, funcname: String = __FUNCTION__) {
        // uncomment this line when app release
        if kLoggingEnabled
        {
            print("****\(filename)(\(line)) \(funcname):\r\(object)\n")
        }
    }
    
    func APILog<T>(object:T)
    {
        if kLoggingEnabled
        {
            debugPrint(object)
        }
    }
}
