//
//  UserProfileManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

let kUserName = "user_name"
let kUserCity = "user_city"
let kUserStateId = "state_id"
let kUserProfileImage = "user_profile_image"


import Foundation

class UserProfileManager
{
    let keyChainWrapper = KeychainWrapper()
    var currentUserProfile:UserProfile?
    var statesList :[StateDetail]?
    
    class var sharedInstance: UserProfileManager {
        struct Static {
            static let instance: UserProfileManager = UserProfileManager()
        }
        return Static.instance
    }
    
    init()
    {
        
    }
    
    func saveUserProfile(email:String,password:String,userProfile:UserProfile)
    {
        LogManager.sharedInstance.DLog("Save user session")
        LogManager.sharedInstance.DLog("Token = \(userProfile.token!),UserRole = \(userProfile.user_role!)")
        currentUserProfile = userProfile
        let hasLoginKey = NSUserDefaults.standardUserDefaults().boolForKey(khasLoginKey)
        if hasLoginKey == false
        {
                keyChainWrapper.mySetObject(password, forKey:kSecValueData)
                NSUserDefaults.standardUserDefaults().setValue(email, forKey: kUserEmail)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.token!, forKey: kUserToken)
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: khasLoginKey)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.user_role!, forKey:kUserRole)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.user_id , forKey: kUserId)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.user_name , forKey: kUserName)
                if let city = userProfile.city
                {
                    NSUserDefaults.standardUserDefaults().setValue( city, forKey: kUserCity)
                }
                if let state = userProfile.state  {
                    NSUserDefaults.standardUserDefaults().setValue(state, forKey: kUserStateId)
                }
                if let image = userProfile.profile_image
                {
                    NSUserDefaults.standardUserDefaults().setValue(image, forKey: kUserProfileImage)
                }
        }
        else
        {
                //Update Values
                keyChainWrapper.mySetObject(password, forKey:kSecValueData)
                NSUserDefaults.standardUserDefaults().setValue(email, forKey: kUserEmail)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.token!, forKey: kUserToken)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.user_role!, forKey:kUserRole)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.user_id , forKey: kUserId)
                NSUserDefaults.standardUserDefaults().setValue(userProfile.user_name , forKey: kUserName)
            
                if let city = userProfile.city
                {
                    NSUserDefaults.standardUserDefaults().setValue( city, forKey: kUserCity)
                }
                if let state = userProfile.state  {
                    NSUserDefaults.standardUserDefaults().setValue(state, forKey: kUserStateId)
                }
                if let image = userProfile.profile_image
                {
                    NSUserDefaults.standardUserDefaults().setValue(image, forKey: kUserProfileImage)
                }
        }
        keyChainWrapper.writeToKeychain()
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func updateUserProfile(userProfile:UserProfile)
    {
        LogManager.sharedInstance.DLog("Save user session")
        LogManager.sharedInstance.DLog("Token = \(userProfile.token!),UserRole = \(userProfile.user_role!)")
        currentUserProfile = userProfile
        //Update Values
        NSUserDefaults.standardUserDefaults().setValue(userProfile.token!, forKey: kUserToken)
        NSUserDefaults.standardUserDefaults().setValue(userProfile.user_role!, forKey:kUserRole)
        NSUserDefaults.standardUserDefaults().setValue(userProfile.user_id , forKey: kUserId)
        NSUserDefaults.standardUserDefaults().setValue(userProfile.user_name , forKey: kUserName)
        
        if let city = userProfile.city
        {
            NSUserDefaults.standardUserDefaults().setValue( city, forKey: kUserCity)
        }
        if let state = userProfile.state  {
            NSUserDefaults.standardUserDefaults().setValue(state, forKey: kUserStateId)
        }
        if let image = userProfile.profile_image
        {
            NSUserDefaults.standardUserDefaults().setValue(image, forKey: kUserProfileImage)
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func clearUserProfile() -> Void
    {
        LogManager.sharedInstance.DLog("Clear session")
        currentUserProfile = nil
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: kUserEmail)
        keyChainWrapper.mySetObject(nil, forKey:kSecValueData)
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: kUserToken)
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: khasLoginKey)
        NSUserDefaults.standardUserDefaults().setValue(nil , forKey: kUserId)
        NSUserDefaults.standardUserDefaults().setValue(nil , forKey: kUserName)
        if let city =  NSUserDefaults.standardUserDefaults().valueForKey(kUserCity) as? String
        {
            NSUserDefaults.standardUserDefaults().setValue( city, forKey: kUserCity)
        }
        if let state =  NSUserDefaults.standardUserDefaults().valueForKey(kUserStateId) as? String
        {
            NSUserDefaults.standardUserDefaults().setValue(state, forKey: kUserStateId)
        }
        if let image = NSUserDefaults.standardUserDefaults().valueForKey(kUserProfileImage) as? String
        {
            NSUserDefaults.standardUserDefaults().setValue(image, forKey: kUserProfileImage)
        }
    }
    
    func getUserProfile() -> UserProfile?
    {
        if currentUserProfile != nil
        {
            return currentUserProfile
        }
        else
        {
            return getSharedUserProfile()
        }
    }
    
    
    func getSharedUserProfile() -> UserProfile
    {
        let userProfile:UserProfile = UserProfile()
        let token :String = NSUserDefaults.standardUserDefaults().valueForKey(kUserToken) as! String
        let userId:String = NSUserDefaults.standardUserDefaults().valueForKey(kUserId) as! String
        let role:String = NSUserDefaults.standardUserDefaults().valueForKey(kUserRole) as! String
        userProfile.user_id = userId
        userProfile.token = token
        userProfile.user_role = role
        userProfile.user_name = NSUserDefaults.standardUserDefaults().valueForKey(kUserName) as? String
        userProfile.city =  NSUserDefaults.standardUserDefaults().valueForKey(kUserCity) as? String
        userProfile.state = NSUserDefaults.standardUserDefaults().valueForKey(kUserStateId) as? String
        userProfile.profile_image = NSUserDefaults.standardUserDefaults().valueForKey(kUserProfileImage) as? String
        return userProfile
    }
    
    
    
}