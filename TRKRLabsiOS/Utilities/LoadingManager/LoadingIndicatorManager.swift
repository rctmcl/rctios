//
//  LoadingIndicatorManager.swift
//  NVActivityIndicatorViewDemo
//
//  Created by Inexgen on 05/01/16.
//  Copyright © 2016 Nguyen Vinh. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class LoadingIndicatorManager {
    var activityIndicatorView:NVActivityIndicatorView!
    let window = UIApplication.sharedApplication().keyWindow
    let animationbgView = UIView()
    let indicatorblurView = UIView()

    class var sharedInstance: LoadingIndicatorManager {
        struct Static {
            static let instance: LoadingIndicatorManager = LoadingIndicatorManager()

        }
        return Static.instance
    }


    init()
    {
    
    }
    
    func showLoadingIndicator()
    {
        if activityIndicatorView == nil
        {
            showindicator()
        }
        else
        {
            hideLoadingIndicator()
            showindicator()
        }
    }
    
    func hideLoadingIndicator()
    {
        if activityIndicatorView != nil
        {
            activityIndicatorView .removeFromSuperview()
            animationbgView.removeFromSuperview()
            indicatorblurView.removeFromSuperview()
            activityIndicatorView.stopAnimation()
            activityIndicatorView = nil
            
        }
    }

    func showindicator()
    {
        activityIndicatorView = NVActivityIndicatorView(frame: (window?.frame)!,type: .LineScale)
        activityIndicatorView.backgroundColor = UIColor.clearColor()
        let width = (window?.frame.size.width)! / 6.4
        activityIndicatorView.color = UIColor.greenColor()
        activityIndicatorView.size = CGSizeMake(width, width)
        indicatorblurView.frame = activityIndicatorView.frame
        indicatorblurView.backgroundColor = UIColor.blackColor()
        indicatorblurView.alpha = 0.5
        animationbgView.frame = CGRectMake(0, 0, width+60, width+50)
        animationbgView.center = activityIndicatorView .convertPoint(activityIndicatorView.center, fromView: activityIndicatorView.superview)
        animationbgView.backgroundColor = UIColor.whiteColor()
        setCornerRadius()
        window?.addSubview(indicatorblurView)
        window?.addSubview(animationbgView)
        window!.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimation()
    }
    func setCornerRadius()
    {
        let bgview = animationbgView.layer
        bgview.cornerRadius = animationbgView.bounds.height / 8
        bgview.masksToBounds = true
        bgview.borderWidth = 1.0
        bgview.borderColor = UIColor.redColor().CGColor
    }
    
}
