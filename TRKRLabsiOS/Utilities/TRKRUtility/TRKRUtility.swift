//
//  TRKRUtility.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 12/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework


enum AthleteRecord:String
{
    case  Effort = "Effort"
    case  Stamina = "Stamina"
    case  SleepQuality = "Sleep Quality"
}

class TRKRUtility
{
    //MARK: Email Validation Function
    class func isValidEmail(emailString:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(emailString)
        return result
    }
    //MARK: Remove Empty rows
    class func removeEmptyCellsInTableView(tableView:UITableView)
    {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    class  func getAthleteStatsColor(athleteRecord:AthleteRecord) -> UIColor
    {
        switch athleteRecord
        {
            case .Effort:
                return EFFORT_COLOR
            case .Stamina:
                return STAMINA_COLOR
            case .SleepQuality:
                return SLEEP_QUALITY_COLOR
        }
    }
    
    class func setBGColor(view:UIView) -> Void
    {
       view.backgroundColor = UIColor(gradientStyle: .Radial , withFrame: view.frame, andColors: [color2,color1])
    }
    class func setCustomBorder(view:UIView)
    {
        let border: UIView = UIView(frame: CGRectMake(0, view.frame.size.height - 1, view.frame.size.width, 1))
        border.backgroundColor = UIColor.whiteColor()
        border.opaque = true
        view.addSubview(border)
    }
    
    //Return error description
    class func getErrorResponse(error:NSError) -> ErrorResponse
    {
        let erResponse = ErrorResponse()
        erResponse.errorDescription = error.description
        erResponse.errorCode = error.code
        return erResponse
    }
    
    //Return API ErrResponse
    class func getAPIErrorResponse(error:BaseObjectMapper) -> ErrorResponse
    {
        let erResponse = ErrorResponse()
        erResponse.errorDescription = error.message
        erResponse.errorCode = error.status
        return erResponse
    }
    

    //MARK: Show Alert Message
    
    class func showAlertMessage(title:String , message:String,viewController:UIViewController)
    {
        let alertView = UIAlertController(title:title ,message: message as String, preferredStyle:.Alert)
        let okAction = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        alertView.addAction(okAction)
        viewController.presentViewController(alertView, animated: true, completion: nil)
    }
    
    //MARK: Get States List
    class func getStateShortName(stateID:String) -> String
    {
        var name :String = ""
        
        if let states = UserProfileManager.sharedInstance.statesList
        {
            for state in states
            {
                if state.state_id == stateID
                {
                    name = state.state_short_name!
                }
            }
        }
        return name
    }
    //MARK: Height Formatter
    class func getFormattedHeight(height:String,page:String) -> String
    {
        if height == "" {
            return height
        }
        var heightString : String = ""
        for char in height.characters
        {
            let catString = String(char)
            if catString == "."{
                if page == "Dashboard" {
                    heightString += " Ft "
                }
                else {
                    heightString += "\'"
                }
                continue
            }
            heightString += catString
        }
        if page == "Dashboard" {
            heightString += " In"
        }
        else {
            heightString += "\""
        }

        print(heightString)
        return heightString
      
    }
    
    
    class func setProfileImage(imageUrl : String?,imageView:UIImageView)
    {
        let bgview = imageView.layer
        bgview.cornerRadius = imageView.bounds.width / 2
        bgview.masksToBounds = true
        bgview.borderWidth = 1.5
        bgview.borderColor = UIColor.whiteColor().CGColor
        
        guard let _ = imageUrl else {
            
            let size = PlaceholderManager.sharedInstance.getCGSizeOfView(view:imageView)
            imageView.image = PlaceholderManager.sharedInstance.getPlaceHolderImageForSize(size: size)
            
            return
        }

        let baseImageUrl =  imageUrl!
        let url = NSURL(string: baseImageUrl)
        imageView.setImageWithURL(url, completed: { (rImage, error, cachetype, nsurl) -> Void in
            if rImage == nil
            {
                let size = PlaceholderManager.sharedInstance.getCGSizeOfView(view:imageView)
                imageView.image = PlaceholderManager.sharedInstance.getPlaceHolderImageForSize(size: size)
            }
            }, usingActivityIndicatorStyle: .Gray)
    }
    
    
    class func setBorder(view:UIView) -> Void
    {
        let layer = view.layer
        layer.borderColor = UIColor.whiteColor().CGColor
        layer.borderWidth = 1
        
    }
    
    class func getFormattedCityName(city:String) ->String
    {
        return city.capitalizeFirst
    }
    
    
   

    
    
}


//MARK: String Utility To Capitalize First Letter
extension String {
    var capitalizeFirst: String {
        if isEmpty { return "" }
        var result = self
        result.replaceRange(startIndex...startIndex, with: String(self[startIndex]).uppercaseString)
        return result
    }
    
}