//
//  PlaceholderManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 17/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

class PlaceholderManager: NSObject
{
    class var sharedInstance: PlaceholderManager {
        struct Static {
            static let instance: PlaceholderManager = PlaceholderManager()
        }
        return Static.instance
    }
    
    override init()
    {
    }
    
    func getPlaceHolderImageForSize(size size:CGSize) -> UIImage
    {
        LogManager.sharedInstance.DLog(size)
        if size.width <= 50
        {
             return UIImage(named: "ProfilePlaceHolderSmall")!
        }
        else if (size.width >= 50 && size.width <= 100)
        {
            return UIImage(named: "DBProfilePlaceholder")!
        }
        return UIImage(named: "DBProfilePlaceholder")!
    }
    
    func getCGSizeOfView(view view:UIView) -> CGSize
    {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }

}
