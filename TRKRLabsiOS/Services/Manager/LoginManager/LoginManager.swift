//
//  LoginManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
class LoginManager: TRKRBaseManager
{
    
    //MARK: Login API
    func login(email:String,password:String,callback:(response: UserProfileResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/auth"
        let params = ["useremail":email,"password":password]
        
        Alamofire.request(.POST, url,parameters:params  ,encoding: .JSON).responseObject { (response: Response<UserProfileResponse, NSError>) in
            
                LogManager.sharedInstance.APILog("response:\(response.debugDescription)")
                
                if response.result.isSuccess
                {
                    self.printAPILog(response.data!)
                    if response.result.value?.status == API_SUCCESS_STATUS_CODE
                    {
                        callback(response: response.result.value!)
                    }
                    else
                    {
                        let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                        failure(error: TRKRUtility.getAPIErrorResponse(error))
                    }
                }
                else if response.result.isFailure
                {
                    failure(error: TRKRUtility.getErrorResponse(response.result.error!))
                }
            }
    }
    
    func logout(token:String,callback:(response: SuccessResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/logoff"
        let params = ["token":token]
        
        Alamofire.request(.POST, url, parameters:params,encoding:  .JSON).responseObject{ (response:Response<SuccessResponse, NSError>) -> Void in
            LogManager.sharedInstance.APILog("response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
  }
    
    
    

}



