//
//  InviteAthleteManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 02/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//
import Foundation
import Alamofire

class InviteAthleteManager: TRKRBaseManager {

    //MARK: GET ATHLETE LIST
    func getAthleteList(searchText:String , userProfile:UserProfile,callback:(response:FansListResponse) -> () ,failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let url = TRKR_BASE_URL + "/atsearch"
        let params = ["token":userProfile.token!,"userid":userProfile.user_id!,"email":searchText]
        
        Alamofire.request(.GET, url,parameters:params  ,encoding:.URL).responseObject { (response: Response<FansListResponse, NSError>) in
            
            LogManager.sharedInstance.APILog("Success: \(response.request) response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
    }
    
    //MARK: Follow Accept / Reject / Delete API
    func AthleteRequestUpdate(userProfile:UserProfile,followingFanDetails:FanDetails,callback:(response:SuccessResponse) -> () ,failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/atrequest"
        let params = ["token":userProfile.token!,"athleteid":followingFanDetails.user_id!,
            "userid":userProfile.user_id!, "status":followingFanDetails.following_status!]
        
        Alamofire.request(.POST, url,parameters:params  ,encoding:.JSON).responseObject { (response: Response<SuccessResponse, NSError>) in
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
        
    }
    
}
