//
//  AthleteStatsManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class AthleteStatsManager: TRKRBaseManager {

    func getAthleteData(userProfile:AthleteUserProfile,callback:(response: AthleteStats)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let url = TRKR_BASE_URL + "/atstats"
        
        guard let _ = userProfile.token , _ = userProfile.user_id , _ = userProfile.startdate , _ = userProfile.enddate , _ = userProfile.type else{
            let error : NSError  = NSError(domain: "Could not process your request. please try again later", code: 403, userInfo: nil)
            failure(error: TRKRUtility.getErrorResponse(error))
            return 
        }
        
        let params = ["token":userProfile.token!,"userid":userProfile.user_id!,"startdate":userProfile.startdate!, "enddate":userProfile.enddate!, "type":userProfile.type!]
        
        
        
        Alamofire.request(.GET, url,parameters:params  ,encoding:.URL).responseObject { (response: Response<AthleteStats, NSError>) in
            
             LogManager.sharedInstance.APILog("Success: \(response.request) response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                let str =  NSString(data: response.data!, encoding: NSUTF8StringEncoding)
                LogManager.sharedInstance.APILog(str)
                
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
    }
    
    
}
