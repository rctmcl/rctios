//
//  ForgotPasswordManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 08/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

import Foundation
import Alamofire

class ForgotPasswordManager : TRKRBaseManager {
    
    //MARK: Send Reset Token
    func sendResetTokenToRegisteredEmail(registeredEmail:String ,callback:(response:SuccessResponse) -> () ,failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let url = TRKR_BASE_URL + "/sendrtoken"
        let params = ["useremail":registeredEmail]
        
        Alamofire.request(.POST, url,parameters:params  ,encoding:.JSON).responseObject { (response: Response<SuccessResponse, NSError>) in
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
    }
    
    //MARK: Check Reset Token
    func checkResetToken(resetToken:String, callback:(response:ProfileDetailResponse) -> () ,failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/checkrtoken"
        let params = ["resettoken":resetToken]
        
        Alamofire.request(.POST, url,parameters:params  ,encoding:.JSON).responseObject { (response: Response<ProfileDetailResponse, NSError>) in
            
            LogManager.sharedInstance.APILog(response.response!)
            LogManager.sharedInstance.APILog(response.request!)
            LogManager.sharedInstance.APILog(response.result)
            
            let str =  NSString(data: response.data!, encoding: NSUTF8StringEncoding)
            LogManager.sharedInstance.APILog(str)
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
        
    }
    
    
    
    
    
    //MARK: Update User Password
    func updateUserPassword(userId:String,userPassword:String, callback:(response:SuccessResponse) -> () ,failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/updatepwd"
        let params = ["userid":userId,"password":userPassword]
        
        Alamofire.request(.POST, url,parameters:params  ,encoding:.JSON).responseObject { (response: Response<SuccessResponse, NSError>) in
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                LogManager.sharedInstance.APILog(response.response!)
                LogManager.sharedInstance.APILog(response.request!)
                LogManager.sharedInstance.APILog(response.result)
                
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
        
    }
}
