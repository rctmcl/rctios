//
//  TRKRBaseManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class TRKRBaseManager
{
    class var sharedInstance: TRKRBaseManager {
        struct Static {
            static let instance: TRKRBaseManager = TRKRBaseManager()
        }
        return Static.instance
    }
    init()
    {
        let rManager = Manager.sharedInstance
        rManager.session.configuration.HTTPAdditionalHeaders = [
            "Content-Type": "application/json"]
    }
    
    func applyMutliplication(value: Int, multFunction: Int -> Int) -> Int {
        return multFunction(value)
    }
    
    
    func printAPILog(data:NSData)
    {
        if kLoggingEnabled
        {
            let str =  NSString(data: data, encoding: NSUTF8StringEncoding)
            LogManager.sharedInstance.APILog(str)
        }
    }
    
   
}
