//
//  FansManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 23/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation

import Alamofire
import ObjectMapper

class FansManager:TRKRBaseManager
{
    
    //MARK: GET Fans List
    func getFansList(userProfile:UserProfile,callback:(response: FansListResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let url = TRKR_BASE_URL + "/myfans"
        let params = ["token":userProfile.token!,"userid":userProfile.user_id!]
        
        Alamofire.request(.GET, url,parameters:params  ,encoding:.URL).responseObject { (response: Response<FansListResponse, NSError>) in
            
            LogManager.sharedInstance.APILog("Success: \(response.request) response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                 self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
        
        
    }
    
    
    
    //MARK: Get States
    
    func getStatesList(callback:(response: StatesListResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let url = TRKR_BASE_URL + "/states"
       
        Alamofire.request(.GET, url,parameters:nil  ,encoding:.URL).responseObject { (response: Response<StatesListResponse, NSError>) in
            
            LogManager.sharedInstance.APILog("response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
        
        
    }
    
    
    
    
    

    
    
    
}