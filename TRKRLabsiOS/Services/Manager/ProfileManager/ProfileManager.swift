//
//  ProfileManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 23/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import Alamofire
//import ObjectMapper

import SwiftyJSON

class ProfileManager:TRKRBaseManager
{
    //MARK : Get Athlete profile
    
    func getAthleteProfile(userProfile:UserProfile,callback:(response: ProfileDetailResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/profile"
        let params = ["token":userProfile.token!,"userid":userProfile.user_id!]
        
        Alamofire.request(.GET, url,parameters:params  ,encoding:.URL).responseObject { (response: Response<ProfileDetailResponse, NSError>) in
            
            LogManager.sharedInstance.APILog("Success: \(response.request) response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }

        
    }
    
    
    func updateAthleteProfile(profileDetails:UserProfileDetails,callback:(response: SuccessResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        let url = TRKR_BASE_URL + "/user"
        
        let params = ["token":profileDetails.token!,"userid":profileDetails.user_id!,"height":profileDetails.user_height!,"weight":profileDetails.user_weight!,"password":profileDetails.user_password!]
        
        Alamofire.request(.POST, url,parameters:params  ,encoding:.JSON).responseObject { (response: Response<SuccessResponse, NSError>) in
            
            LogManager.sharedInstance.APILog("Success: \(response.request) response:\(response.debugDescription)")
            
            if response.result.isSuccess
            {
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }

        
    }
    
    
        
    
    
    func uploadImage(profileDetails:UserProfile,callback:(response: SuccessResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let myUrl = NSURL(string:TRKR_BASE_URL + "/proimageupdate")
        
        let request = NSMutableURLRequest(URL:myUrl!);
        request.HTTPMethod = "POST";
        
        let param = [
            "token"     : profileDetails.token!,
            "userid"    : profileDetails.user_id!
        ]
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        if profileDetails.profileImageData == nil {
            return
        }
        
        let imageData = profileDetails.profileImageData!
        
        request.HTTPBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData, boundary: boundary)
        
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                dispatch_async(dispatch_get_main_queue(),{
                    let failureBlock = ErrorResponse()
                    failure(error: failureBlock)
                })
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("****** response data = \(responseString!)")
            
           
        
           // var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err) as? NSDictionary
         /*
            var err: NSError
            // throwing an error on the line below (can't figure out where the error message is)
            var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
            print(jsonResult)
            */
            let json = JSON(response!)
            
            print("json = \(json)")
            let succesBlock = SuccessResponse()
            
            
            dispatch_async(dispatch_get_main_queue(),{
               callback(response: succesBlock)
            });
        }
        
        task.resume()
        
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.appendData(imageDataKey)
        body.appendString("\r\n")
        
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().UUIDString)"
    }
    
    
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}
