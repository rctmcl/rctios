//
//  DashboardManager.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class DashboardManager:TRKRBaseManager
{
    
    
    func getDashboardData(userProfile:UserProfile,callback:(response: AthleteDashboardResponse)-> (),failure:(error : ErrorResponse)-> ()) -> Void
    {
        
        let url = TRKR_BASE_URL + "/atdashboard"
        let params = ["token":userProfile.token!,"userid":userProfile.user_id!,"date":userProfile.date!]
        
        Alamofire.request(.GET, url,parameters:params  ,encoding:.URL).responseObject { (response: Response<AthleteDashboardResponse, NSError>) in
            
            LogManager.sharedInstance.APILog("response:\(response.debugDescription)")
            
            let str =  NSString(data: response.data!, encoding: NSUTF8StringEncoding)
             LogManager.sharedInstance.APILog(str)
            
            if response.result.isSuccess
            {
                self.printAPILog(response.data!)
                if response.result.value?.status == API_SUCCESS_STATUS_CODE
                {
                    callback(response: response.result.value!)
                }
                else
                {
                    let error :BaseObjectMapper = response.result.value! as BaseObjectMapper
                    failure(error: TRKRUtility.getAPIErrorResponse(error))
                }
            }
            else if response.result.isFailure
            {
                failure(error: TRKRUtility.getErrorResponse(response.result.error!))
            }
        }
    }
    
    
}