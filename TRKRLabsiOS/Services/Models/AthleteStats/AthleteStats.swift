//
//  AthleteStats.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class AthleteStats:BaseObjectMapper
{
    var result:[AthleteStatsData]?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map)
    {
        super.init(map)
    }
    override func mapping(map: Map) {
        super.mapping(map)
        result  <- map["result"]
    }
    
}


class AthleteStatsData:Mappable
{
    var week_day:String?
    var ess_date:String?
    var effort_percent:String?
    var stamina_percent:String?
    var sleep_percent:String?
    var day_no:String?
    var month_no:String?
    var type : String?
    
    init()
    {
    }
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        
        week_day            <- map["week_day"]
        ess_date            <- map["ess_date"]
        effort_percent      <- map["effort_percent"]
        stamina_percent     <- map["stamina_percent"]
        sleep_percent       <- map["sleep_percent"]
        day_no              <- map["day_no"]
        month_no            <- map["month_no"]
        type                <- map["type"]
    }
}


