//
//  ProfileDetailResponse.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 23/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileDetailResponse: BaseObjectMapper {
    
    var result:[UserProfileDetails]?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map)
    {
        super.init(map)
    }
    override func mapping(map: Map) {
        super.mapping(map)
        result  <- map["result"]
    }
    
}


class  UserProfileDetails : Mappable
{
    var user_id:String?
    var user_name:String?
    var user_height:String?
    var user_weight:String?
    var user_profile_image:String?
    var user_email:String?
    var user_password:String?
    var user_role:String?
    var user_type:String?
    var user_status:String?
    var city:String?
    var state_id:String?
    var user_dob:String?
    var user_sport:String?
    var user_gender:String?
    var position:String?
    var user_favorite_team:String?
    var user_phone : String?
    var token:String?
   
    init()
    {
    }
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        
        user_id             <- map["user_id"]
        user_name           <- map["user_name"]
        user_height         <- map["user_height"]
        user_weight         <- map["user_weight"]
        user_profile_image  <- map["user_profile_image"]
        user_email          <- map["user_email"]
        user_password       <- map["user_password"]
        user_role           <- map["user_role"]
        user_type           <- map["user_type"]
        user_status         <- map["user_status"]
        city                <- map["city"]
        state_id            <- map["state_id"]
        user_dob            <- map["user_dob"]
        user_sport          <- map["user_sport"]
        user_gender         <- map["user_gender"]
        position            <- map["position"]
        user_favorite_team  <- map["user_favorite_team"]
        user_phone          <- map["user_phone"]
    }
}