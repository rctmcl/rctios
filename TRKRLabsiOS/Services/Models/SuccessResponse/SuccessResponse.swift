//
//  SuccessResponse.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class SuccessResponse: BaseObjectMapper {

    var result:AnyObject?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map){
        super.init(map)
    }
    override func mapping(map: Map) {
        super.mapping(map)
        result <- map["result"]
    }

}
