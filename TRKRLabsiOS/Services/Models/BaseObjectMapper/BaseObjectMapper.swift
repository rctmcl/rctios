//
//  BaseObjectMapper.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseObjectMapper:Mappable
{
    var status:Int?
    var message:String?
    
    init()
    {
        
    }
    required init?(_ map: Map){
        
    }
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
    }
}
