//
//  UserProfile.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper
class UserProfileResponse: BaseObjectMapper {
    
    var result:UserProfile?
    
    override init()
    {
       super.init()
    }
    required init?(_ map: Map)
    {
        super.init(map)
    }
   override func mapping(map: Map) {
        super.mapping(map)
        result  <- map["result"]
    }
    
}

class  UserProfile:Mappable
{
    var token:String?
    var user_id:String?
    var user_name:String?
    var user_role:String?
    var profile_image:String?
    var city:String?
    var state:String?
    var date:String?
    var profileImageData: NSData?
    
    init()
    {
    }
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        user_id <- map["user_id"]
        user_name <- map["user_name"]
        user_role <- map["user_role"]
        profile_image <- map["profile_image"]
        city <- map["city"]
        state <- map["state"]
        
    }
}

class AthleteUserProfile:UserProfile
{
    var startdate : String?
    var enddate : String?
    var type : String?
    
    override func mapping(map: Map) {
        token       <- map["token"]
        user_id     <- map["user_id"]
        startdate   <- map["startdate"]
        enddate     <- map["enddate"]
    }
    
}

