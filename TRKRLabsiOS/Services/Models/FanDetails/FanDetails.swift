//
//  FanDetails.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 23/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper


class FansListResponse:BaseObjectMapper
{
    var result:[FanDetails]?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map)
    {
        super.init(map)
    }
    override func mapping(map: Map) {
        super.mapping(map)
        result  <- map["result"]
    }
}



class FanDetails:Mappable
{
    var user_id:String?
    var user_name:String?
    var city:String?
    var state_id:String?
    var athelete_fan_id:String?
    var following_status:String?
    var profile_image:String?
    
    init()
    {
    }
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        user_id             <- map["user_id"]
        user_name           <- map["user_name"]
        city                <- map["city"]
        state_id            <- map["state_id"]
        athelete_fan_id     <- map["athelete_fan_id"]
        following_status    <- map["following_status"]
        profile_image       <- map["user_profile_image"]
    }
}