//
//  ErrorResponse.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class ErrorResponse:BaseObjectMapper
{
    var errorCode:Int?
    var errorDescription:String?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map){
        super.init(map)
    }
    override func mapping(map: Map) {
        errorCode <- map["errorCode"]
        errorDescription <- map["errorDescription"]
    }
}