//
//  AthleteDashboardResponse.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 22/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper
class AthleteDashboardResponse:BaseObjectMapper
{
    var result:[DashboardData]?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map)
    {
        super.init(map)
    }
    override func mapping(map: Map) {
        super.mapping(map)
        result  <- map["result"]
    }

}


class DashboardData:Mappable
{
    var user_id:String?
    var user_name:String?
    var user_height:String?
    var user_weight:String?
    var user_profile_image:String?
    var effort_percent:String?
    var stamina_percent:String?
    var sleep_percent:String?
    var sedentary_minutes:String?
    var resting_heart_rate:String?
    var active_minutes:String?
    var peak_minutes:String?
    
    init()
    {
    }
    
    required init?(_ map: Map){
    }
    
    func mapping(map: Map) {
        user_id             <- map["user_id"]
        user_name           <- map["user_name"]
        user_height         <- map["user_height"]
        user_weight         <- map["user_weight"]
        user_profile_image  <- map["user_profile_image"]
        effort_percent      <- map["effort_percent"]
        stamina_percent     <- map["stamina_percent"]
        sleep_percent       <- map["sleep_percent"]
        sedentary_minutes   <- map["sedentary_minutes"]
        resting_heart_rate  <- map["resting_heart_rate"]
        active_minutes      <- map["active_minutes"]
        peak_minutes        <- map["peak_minutes"]
    }
}