//
//  StatesResponse.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 23/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import ObjectMapper

class StatesListResponse:BaseObjectMapper
{
    var result:[StateDetail]?
    
    override init()
    {
        super.init()
    }
    required init?(_ map: Map)
    {
        super.init(map)
    }
    override func mapping(map: Map) {
        super.mapping(map)
        result  <- map["result"]
    }
}



class StateDetail:Mappable
{
    
    var state_id:String?
    var state_name:String?
    var state_short_name:String?
    
    init()
    {
    }
    required init?(_ map: Map)
    {
    }

    
    func mapping(map: Map)
    {
        state_id             <- map["state_id"]
        state_name           <- map["state_name"]
        state_short_name     <- map["state_short_name"]
    }
}