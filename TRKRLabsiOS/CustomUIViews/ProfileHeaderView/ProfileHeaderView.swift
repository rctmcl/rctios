//
//  ProfileHeaderView.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 19/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

protocol ProfileHeaderViewDelegate {
    func editProfileButtonDelegate(sender : UIButton)
}


class ProfileHeaderView: UIView {
    
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var editButton: UIButton!
    var delegate : ProfileHeaderViewDelegate?
    
    //MARK : INIT METHOD
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
        
    }
    //MARK : Decoder Init METHOD
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    private func commonInit() {
        NSBundle.mainBundle().loadNibNamed("ProfileHeaderView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        //contentView.backgroundColor = UIColor(gradientStyle: .TopToBottom, withFrame:contentView.frame , andColors: [bgColor1,bgColor2])
        self.addSubview(content)
    }
   
    //MARK : Profile Button Action
    
    @IBAction func profileButtonTapped(sender: UIButton) {
        
        if let tDelegate = delegate {
         tDelegate.editProfileButtonDelegate(sender)
        }
    
    }
    
    
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
