//
//  CustomCircleProgressView.swift
//  SampleCircleProgressView
//
//  Created by inexgen on 16/12/15.
//  Copyright © 2015 River Cities Technology. All rights reserved.
//

import UIKit
import CircleProgressView

class CustomCircleProgressView: UIView {
    let trackBackgroundColor = UIColor(red: 113.0/255.0, green: 141.0/255.0, blue: 179.0/255.0, alpha: 0.75)
    //let trackBackgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let percentLabelColor =  UIColor(red:215.0/255.0, green: 210.0/255.0, blue: 203.0/255.0, alpha: 1.0)
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var customProgressView: CircleProgressView!
    @IBOutlet var statsTitleLabel: UILabel!
    @IBOutlet var statsPercentLabel: UILabel!
    
    
    
    
    required override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        NSBundle.mainBundle().loadNibNamed("CustomCircleProgressView", owner: self, options: nil)
        self.addSubview(self.contentView)
        setCircleProgressValue()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        NSBundle.mainBundle().loadNibNamed("CustomCircleProgressView", owner: self, options: nil)
        self.addSubview(self.contentView)
        setCircleProgressValue()
    }
    
    
    func setCircleProgressValue() -> Void
    {
        if customProgressView != nil
        {
            customProgressView.trackBackgroundColor = trackBackgroundColor
            customProgressView.centerFillColor = DONUT_CENTER_COLOR
            statsPercentLabel.textColor = percentLabelColor
        }
    }
    
    
    func setDountValues(percent:String,aRecord:AthleteRecord) -> Void
    {
        statsTitleLabel.text = aRecord.rawValue
        statsPercentLabel.text = percent + "%"
        customProgressView.progress = Double(percent)!/100
        statsTitleLabel.textColor = TRKRUtility.getAthleteStatsColor(aRecord)
        customProgressView.trackFillColor = TRKRUtility.getAthleteStatsColor(aRecord)
        customProgressView.trackBackgroundColor = trackBackgroundColor
        customProgressView.trackWidth = 2
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
