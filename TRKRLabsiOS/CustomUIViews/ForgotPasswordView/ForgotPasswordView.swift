//
//  ForgotPasswordView.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 07/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var submitButton: UIButton!
    
    
    @IBOutlet var emailView: UIView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK : INIT METHOD
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
        
    }
    //MARK : Decoder Init METHOD
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    private func commonInit() {
        NSBundle.mainBundle().loadNibNamed("ForgotPasswordView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        self.addSubview(content)
        TRKRUtility.setBorder(emailView)
    }
}
