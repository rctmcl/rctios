//
//  TemporaryPasswordView.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 07/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

import UIKit

class TemporaryPasswordView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var temporaryPasswordView: UIView!
    @IBOutlet var tempPasswordTextField: UITextField!
    
    @IBOutlet var passwordSubmitButton: UIButton!
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    //MARK : Decoder Init METHOD
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    private func commonInit() {
        NSBundle.mainBundle().loadNibNamed("TemporaryPasswordView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        self.addSubview(content)
        TRKRUtility.setBorder(temporaryPasswordView)
    }
    
    
  


}
