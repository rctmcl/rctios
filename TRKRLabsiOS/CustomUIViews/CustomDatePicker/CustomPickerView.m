
#import "CustomPickerView.h"
#import "Masonry.h"
#import "Chameleon.h"

#define ROW_HEIGHT 34
#define DIV_HEIGHT 34


@interface CustomPickerView(PrivateMethods)

- (UIImage *)addText:(UIImage *)img text:(NSString *)text;
- (UIImage *)imageWithColor:(UIColor *)color forRect:(CGRect) rect;

@end

@implementation CustomPickerView

@synthesize tableView, strings, delegate;
@synthesize isSpinning = _isSpinning;
@synthesize selectedString = _selectedString;
@synthesize selectedIndex = _selectedIndex;
@synthesize verticalLabelOffset = _verticalLabelOffset;
@synthesize labelFontSize = _labelFontSize;
@synthesize data4Rows = _data4Rows;
@synthesize bFirstTime;
@synthesize trkrFont;

#pragma mark - init functions

- (id)initWithFrame:(CGRect)frame
         background:(UIImage*)backImage
itemVerticalOffset:(CGFloat)offset andData:(NSArray*) data
{
    CGRect rect;
    rect.origin.x = frame.origin.x;
    rect.origin.y = frame.origin.y;
    rect.size.width = backImage.size.width;
    rect.size.height = backImage.size.height;
   
    
    self = [super initWithFrame:CGRectZero];
    
    if (self)
    {
        self.verticalLabelOffset = offset;
        self.isSpinning = NO;
        isAnimating = NO;
        bFirstTime = YES;
        
        UIColor *bgColor = [UIColor colorWithRed: 13.0/255.0 green:55.0/255.0 blue:135.0/255.0 alpha:1.0];
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.rowHeight = ROW_HEIGHT;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.showsHorizontalScrollIndicator = NO;
        self.tableView.separatorColor = [UIColor clearColor];
        self.tableView.backgroundColor = [UIColor clearColor];
        
        self.backgroundColor = bgColor;
        
       /* CALayer *cLayer = self.layer;
        cLayer.borderColor = [UIColor whiteColor].CGColor;
        cLayer.borderWidth = 0.5;
*/     //   self.tableView.estimatedRowHeight = ROW_HEIGHT;
     //   self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        trkrFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
        
        UIImageView *overlayView = [[UIImageView alloc] initWithImage:backImage];
        overlayView.center = CGPointMake(rect.size.width/2, rect.size.height/2);
        
        self.tableView.backgroundView = overlayView;// this depends how u would like add background vie
        [self addSubview:self.tableView]; //on base image
        
        UIEdgeInsets padding = UIEdgeInsetsMake(0,0,0,-2);
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(padding);
        }];
        
         self.data4Rows = data;
        
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        //[self snap];
    }
    return self;
}




#pragma mark - memory managment

- (void)dealloc
{
    self.customPickerViewControllerDidSpinCallback = nil;
    self.tableView = nil;
    self.data4Rows = nil;
    self.strings = nil;
}


#pragma mark - UITableViev delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView = nil;
    if (section == 0)
    {
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width,(self.bounds.size.height/DIV_HEIGHT))];
       // [headerView setBackgroundColor:[UIColor yellowColor]];
    }
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* headerView = nil;
    if (section == 0)
    {
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width,(self.bounds.size.height/DIV_HEIGHT))];
       // [headerView setBackgroundColor:[UIColor redColor]];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return (self.bounds.size.height/DIV_HEIGHT);
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        return (self.bounds.size.height/DIV_HEIGHT);
    }
    return 0;
}
 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _data4Rows.count;
}

#pragma mark - UITableVievDataSourse

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellName = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    UITableViewCell *cell =  nil;//(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:cellName];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        DateDataStructure *dStruct =[self.data4Rows objectAtIndex:indexPath.row];
        NSString* dataObjectStr = [NSString stringWithFormat:@"%@",dStruct.displayString];
     /*
        UIImage* image = [self addText:[self imageWithColor:[UIColor redColor]forRect:CGRectMake(0, 0,cell.frame.size.width, 30)] text: dataObjectStr];
        UIImageView* numberImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, cell.frame.size.width-20, ROW_HEIGHT)];
        numberImage.contentMode = UIViewContentModeCenter;
        numberImage.image = image;
        [cell.contentView addSubview:numberImage];
       */
        cell.textLabel.font = trkrFont;
        cell.textLabel.text = dataObjectStr;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor whiteColor];
    }

    
    return cell;
}



#pragma mark UIScrollViewDelegate methods

-(void)retrieveCustomPickerViewControllerDidSpinCallback:(CustomPickerViewControllerDidSpinCallback)callback
{
    self.customPickerViewControllerDidSpinCallback = callback;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.isSpinning = YES;
    [self.delegate pickerControllerDidSpin:self];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        self.isSpinning = NO;
        isAnimating = NO;
        [self snap];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.isSpinning = NO;
    isAnimating = NO;
    [self snap];
    
    CGPoint offset = scrollView.contentOffset;
    if (offset.y < 5) {
        [self scrollViewDidEndScrollingAnimation:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (isAnimating)
    {
        isAnimating = NO;
        self.isSpinning = NO;
        [self.delegate pickerController:self didSnapToString:self.selectedString];
        if (self.customPickerViewControllerDidSpinCallback)
        {
            self.customPickerViewControllerDidSpinCallback((int)(self.selectedIndex + 1));
        }

    }
    else
        [self snap];
}

#pragma mark - private methods

- (void)setDataIndex:(NSUInteger)index
{
    int rowsCount =(int) [self.tableView numberOfRowsInSection:0];
    _selectedIndex = index < rowsCount ? index : (rowsCount-1)>0 ? (rowsCount-1) : 0 ;
    
    [self.tableView  reloadData];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    
    if (bFirstTime == YES) {
        bFirstTime = NO;
    }
}

-(void)setData4Rows:(NSArray *)data4Rows
{
    _data4Rows = data4Rows;
    [self.tableView reloadData];
    [self snap];
}

- (void)snap
{
    if (isAnimating)
        return;
    
    isAnimating = YES;
    
    self.isSpinning = NO;
    
    double verticalPadding = (self.tableView.frame.size.height - self.tableView.rowHeight) * .5;
    
    // NSLog(@"vertical Padding =%f",verticalPadding);
    
    for (int i=0; i<[[self.tableView visibleCells] count]; i++)
    {
        UITableViewCell *cell = [[self.tableView visibleCells] objectAtIndex:i];
        
        CGRect tableCellRect = CGRectMake(0, self.tableView.contentOffset.y + verticalPadding,
                                          self.tableView.frame.size.width, self.tableView.rowHeight);
        
       // NSLog(@"tableCellRect =%@, cell.Center =%@",NSStringFromCGRect(tableCellRect), NSStringFromCGPoint(cellCenter));
        
        BOOL selected = CGRectContainsPoint( tableCellRect,cell.center);
        if (selected)
        {
           // NSLog(@"selelcted ");
            
            isAnimating = YES;
            self.isSpinning = NO;
            
            [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            _selectedIndex = [self.tableView indexPathForCell:cell].row;
            if ([self.tableView rectForRowAtIndexPath:[self.tableView indexPathForCell:cell]].origin.y == self.tableView.contentOffset.y + (self.tableView.frame.size.height - ROW_HEIGHT) * .5)
            {
                _selectedIndex = [self.tableView indexPathForCell:cell].row;
                [self.delegate pickerController:self didSnapToString:self.selectedString];
                isAnimating = NO;
            }
        }
    }
}


-(UIImage *)addText:(UIImage *)img text:(NSString *)text
{
    
    CGPoint point = CGPointMake(10.0f, 3.0f);
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    UIGraphicsBeginImageContext(img.size);
    [img drawInRect:CGRectMake(0,0,img.size.width,img.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, img.size.width, img.size.height);
    [[UIColor whiteColor] set];
    
    if ([text respondsToSelector:@selector(drawInRect:withAttributes:)]) {
        NSDictionary *attributes = @{ NSFontAttributeName: font,
                                      NSForegroundColorAttributeName: [UIColor whiteColor]};
        [text drawInRect:CGRectIntegral(rect) withAttributes:attributes];
    }
    else
    {
        //[text drawInRect:CGRectIntegral(rect) withFont:font];
        
        // Create text attributes
        NSDictionary *textAttributes = @{NSFontAttributeName: font};
        
        // Create string drawing context
        NSStringDrawingContext *drawingContext = [[NSStringDrawingContext alloc] init];
        drawingContext.minimumScaleFactor = 0.5; // Half the font size
    
        [text drawWithRect:rect
                     options:NSStringDrawingUsesLineFragmentOrigin
                  attributes:textAttributes
                     context:drawingContext];
        
    }
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

- (UIImage *)imageWithColor:(UIColor *)color forRect:(CGRect) rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
