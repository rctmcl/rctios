//
//  UICustomDatePicker.m
//  Seldon_2
//
//  Created by admin on 12.08.13.
//  Copyright (c) 2013 Zorin Evgeny. All rights reserved.
//

#import "CustomDatePicker.h"
#import "CustomPickerView.h"
#import "Masonry.h"

#define MIN_YEAR_VALUE 1900 

@interface  CustomDatePicker() <CustomPickerControllerDelegate>
{
    CustomPickerView* _dayPicker;
    CustomPickerView* _yearPicker;
    CustomPickerView* _mounthPicker;
    CustomPickerView* _weekPicker;
    NSInteger _year;
    
    NSInteger _minYear;
    
}

@property(strong,retain)UIImage* dayImage;
@property(strong,retain)UIImage* monthImage;
@property(strong,retain)UIImage* yearImage;
@property(strong,retain)UIImage* weekImage;
-(void) defaultDataInit;
 
@end


@implementation CustomDatePicker

@synthesize dayImage = _dayImage;
@synthesize monthImage = _monthImage;
@synthesize yearImage = _yearImage;
@synthesize weekImage = _weekImage;
@synthesize delegate;
@synthesize currentCalendarStyle;

#pragma mark - Init functions

-(id)initWithCoder:(NSCoder *)aDecoder
{
    
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self defaultDataInit];
    }
    return self;

}


-(id)init
{
    if (self = [super init])
    {
        [self defaultDataInit];
    }
    
    return self;
}
-(id)initWithImageForDay:(UIImage*)dayImage andMonthImage:(UIImage*)monthImage andYearImage:(UIImage*)yearImage forRect:(CGRect)rect
{
    self = [self initWithFrame:rect];
    
    if (self)
    {
        _dayImage = dayImage;
        _monthImage = monthImage;
        _yearImage = yearImage;
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self defaultDataInit];
        
    }
    return self;
}
-(NSTimeZone *)getTimeZone
{
    return [NSTimeZone localTimeZone];
}

-(void) defaultDataInit
{
    //Override
    [self defaultCalendarConfig];
    
    _dayPicker  = nil;
    _yearPicker = nil;
    _mounthPicker = nil;
    _weekPicker = nil;
    
    _year = MIN_YEAR_VALUE;
    
    _calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    _date = [NSDate date];
    _minYear = MIN_YEAR_VALUE;
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:1];
    [comps setYear:MIN_YEAR_VALUE];
    
    _minimumDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    _maximumDate = [NSDate date];
 
   
         
}

-(void)defaultCalendarConfig
{
    //Set Default Calendar Style to Daily
    currentCalendarStyle = CalendarStyle_Daily;
}

-(void)removeAllSubViews
{
    NSArray *subViews  = self.subviews;
    for (UIView *view in subViews) {
        [view removeFromSuperview];
    }
}

-(void)configCalendarComponent
{
    [self removeAllSubViews];
    switch (currentCalendarStyle) {
        case CalendarStyle_Daily:
            [self DailyCalendarConfiguration];
            break;
        case CalendarStyle_Weekly:
             [self WeeklyCalendarConfiguration];
            break;
        case CalendarStyle_Monthly:
             [self MonthlyCalendarConfiguration];
            break;
        case CalendarStyle_Yearly:
             [self YearlyCalendarConfiguration];
            break;
        default:
            break;
    }
    
    
   
    
}

#pragma mark Calendar Configuration-Daily,weekly,monthly,yearly

-(void)DailyCalendarConfiguration
{
    _monthImage = [UIImage imageNamed:@"CalendarMonthImage"];
    _yearImage  = [UIImage imageNamed:@"CalendarMonthImage"];
    _dayImage  = [UIImage imageNamed:@"CalendarMonthImage"];
    
    //Year Picker Config
    [self yearPickerConfiguration:CGRectZero];
    
    // Month Picker
    [self monthPickerConfiguration:CGRectZero];
    
    
    //Day Picker
    [self dayPickerConfiguration:CGRectZero];
    
    
    [self addSubview:_dayPicker];
    [self addSubview:_mounthPicker];
    [self addSubview:_yearPicker];

    [self dailyCalendarUpdateConstraints];
    
    
    
}

-(void)dailyCalendarUpdateConstraints
{
    UIEdgeInsets padding = UIEdgeInsetsMake(1, 1, 1, 1);
     UIView *superview = self;
   
    [_mounthPicker mas_makeConstraints:^(MASConstraintMaker *make) {
     make.top.equalTo(superview.mas_top).with.offset(padding.top);
     make.left.equalTo(superview.mas_left).with.offset(padding.left);
     make.width.equalTo(superview.mas_width).dividedBy(3.0);
     make.height.equalTo(superview.mas_height);
     }];
     
     [_dayPicker mas_makeConstraints:^(MASConstraintMaker *make) {
     make.top.equalTo(superview.mas_top).with.offset(padding.top);
     make.left.equalTo(_mounthPicker.mas_right).with.offset(padding.left);
     make.width.equalTo(_mounthPicker.mas_width).dividedBy(0.9);
     make.height.equalTo(_mounthPicker);
     }];
     
     [_yearPicker mas_makeConstraints:^(MASConstraintMaker *make) {
     make.top.equalTo(superview.mas_top).with.offset(padding.top);
     make.left.equalTo(_dayPicker.mas_right).with.offset(padding.left);
     make.width.equalTo(_mounthPicker.mas_width).dividedBy(1.2);
     make.height.equalTo(_mounthPicker);
     }];
    
}


-(void)WeeklyCalendarConfiguration
{
   // _monthImage = [UIImage imageNamed:@"CalendarMonthImage"];
    _yearImage  = [UIImage imageNamed:@"CalendarMonthImage"];
    _weekImage  = [UIImage imageNamed:@"CalendarMonthImage"];
    
    // Year Picker
    [self yearPickerConfiguration:CGRectZero];
    
    // Month Picker
  //  [self monthPickerConfiguration:CGRectZero];
    
    //Week Picker
    [self weekPickerConfiguration:CGRectZero];
    
    [self addSubview:_weekPicker];
   // [self addSubview:_mounthPicker];
    [self addSubview:_yearPicker];
    
    [self weeklyCalendarUpdateConstraints];
}

-(void)weeklyCalendarUpdateConstraints
{
    UIEdgeInsets padding = UIEdgeInsetsMake(1, 1, 1, 1);
    UIView *superview = self;
    
   /* [_mounthPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(superview.mas_left).with.offset(padding.left);
        make.width.equalTo(superview.mas_width).dividedBy(3.0);
        make.height.equalTo(superview.mas_height);
    }];
    */
    
    [_weekPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(superview.mas_left).with.offset(padding.left);
        make.width.equalTo(superview.mas_width).dividedBy(1.41);
        make.height.equalTo(superview.mas_height);
    }];
    
    [_yearPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(_weekPicker.mas_right).with.offset(padding.left);
        make.width.equalTo(_weekPicker.mas_width).dividedBy(2.57);
        make.height.equalTo(_weekPicker);
    }];
    

}


-(void)MonthlyCalendarConfiguration
{
    _monthImage = [UIImage imageNamed:@"CalendarMonthImage"];
    _yearImage  = [UIImage imageNamed:@"CalendarMonthImage"];
    
    //Year Picker Config
    [self yearPickerConfiguration:CGRectZero];
    
    // Month Picker
    [self monthPickerConfiguration:CGRectZero];
    
    [self addSubview:_mounthPicker];
    [self addSubview:_yearPicker];
    
    [self monthlyCalendarUpdateConstraints];
}


-(void)monthlyCalendarUpdateConstraints
{
    UIEdgeInsets padding = UIEdgeInsetsMake(1, 1, 1, 1);
    UIView *superview = self;
    
    UIView *leftSpacerView  = [[UIView alloc] initWithFrame:CGRectZero];
    UIView *rightSpacerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:leftSpacerView];
    [self addSubview:rightSpacerView];
    
    [leftSpacerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(superview.mas_left).with.offset(padding.left);
        make.width.equalTo(rightSpacerView.mas_width);
        make.height.equalTo(superview.mas_height);
    }];
    
    
    [_mounthPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(leftSpacerView.mas_right).with.offset(padding.left);
        make.width.equalTo(superview.mas_width).dividedBy(3);
        make.height.equalTo(superview.mas_height);
    }];
    
    [_yearPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(_mounthPicker.mas_right).with.offset(padding.left);
        make.width.equalTo(_mounthPicker.mas_width);
        make.height.equalTo(_mounthPicker);
    }];
    
    [rightSpacerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(_yearPicker.mas_right).with.offset(padding.left);
        make.right.equalTo(superview.mas_right).with.offset(padding.right);
        make.width.equalTo(leftSpacerView.mas_width);
        make.height.equalTo(superview.mas_height);
    }];
    
    
}


-(void)YearlyCalendarConfiguration
{
    _yearImage  = [UIImage imageNamed:@"CalendarMonthImage"];
    
    //Year Picker Config
    [self yearPickerConfiguration:CGRectZero];
    
    [self addSubview:_yearPicker];
    
    [self yearlyCalendarUpdateConstraints];
}



-(void)yearlyCalendarUpdateConstraints
{
    UIEdgeInsets padding = UIEdgeInsetsMake(1, 1, 1, 1);
    UIView *superview = self;
    
    UIView *leftSpacerView  = [[UIView alloc] initWithFrame:CGRectZero];
    UIView *rightSpacerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:leftSpacerView];
    [self addSubview:rightSpacerView];
    
    [leftSpacerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(superview.mas_left).with.offset(padding.left);
        make.width.equalTo(rightSpacerView.mas_width);
        make.height.equalTo(superview.mas_height);
    }];
    
    [_yearPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(leftSpacerView.mas_right).with.offset(padding.left);
        make.width.equalTo(superview.mas_width).dividedBy(2.5);
        make.height.equalTo(superview.mas_height);
    }];
    
    [rightSpacerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).with.offset(padding.top);
        make.left.equalTo(_yearPicker.mas_right).with.offset(padding.left);
        make.right.equalTo(superview.mas_right).with.offset(padding.right);
        make.width.equalTo(leftSpacerView.mas_width);
        make.height.equalTo(superview.mas_height);
    }];
}



#pragma mark Year,Month,Day,Week Picker Data

-(NSMutableArray *)getYearPickerData
{
    if (_maximumDate < _date){
        _date = _maximumDate;
    }
    NSDateComponents *components = [_calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:_maximumDate];
    NSInteger year = [components year];
    NSMutableArray* years = [NSMutableArray array];
    _minYear = [[_calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:_minimumDate] year];
    for (int i = (int)_minYear; i<=year; i++)
    {
        DateDataStructure *dStruct = [[DateDataStructure alloc] init];
        dStruct.displayString = [NSString stringWithFormat:@"%d",i];
        [years addObject:dStruct];
    }
    return years;
}

-(NSArray *)getMonthPickerData
{
    NSDateFormatter *df = [NSDateFormatter new];
    NSArray *monthNames = [df shortStandaloneMonthSymbols];
    NSMutableArray *monthNameStructArray = [NSMutableArray array];
    for (NSString *monthString in monthNames) {
        DateDataStructure *dStruct = [[DateDataStructure alloc] init];
        dStruct.displayString = monthString;
        [monthNameStructArray addObject:dStruct];
    }
    return monthNameStructArray;
}

-(NSMutableArray *)getDayPickerData
{
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    return [self getDaysStructArrayFor:comps];
}

-(NSArray *)getWeeklyPickerData
{
    NSDate *newDate = [NSDate date];
    NSArray *weekNames = [self getFullListOfData:newDate];
    return weekNames;
}


-(NSMutableArray *)getDaysStructArrayFor:(NSDateComponents *)component
{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDate *date = [cal dateFromComponents:component];
    [cal components:(NSCalendarUnitDay | NSCalendarUnitMonth) fromDate:date];
    
    NSRange range = [cal rangeOfUnit:NSCalendarUnitDay
                              inUnit:NSCalendarUnitMonth
                             forDate:date];//[cal dateFromComponents:component]];
    //  NSLog(@"%d", range.length);
    NSMutableArray* days = [NSMutableArray array];
    for (int i= 1; i<=range.length; i++)
    {
        DateDataStructure *dStruct = [[DateDataStructure alloc] init];
        dStruct.displayString = [NSString stringWithFormat:@"%d",i];
        [days addObject:dStruct];
    }
    return days;
}

-(NSInteger )getChosenYearFromPicker
{
    DateDataStructure *yearStruct = [_yearPicker.data4Rows objectAtIndex:_yearPicker.selectedIndex];
    return [yearStruct.displayString integerValue];
}

-(NSInteger)getChosenMonthFromPicker
{
    return (_mounthPicker.selectedIndex+1);
}

#pragma mark - Picker Configuration
-(void)yearPickerConfiguration:(CGRect)yearPkrFrame
{
    // Year Picker
    _yearPicker= [[CustomPickerView alloc] initWithFrame:CGRectZero background:_yearImage itemVerticalOffset:0.0f andData:[self getYearPickerData]];
    _yearPicker.translatesAutoresizingMaskIntoConstraints = NO;
    _yearPicker.delegate = self;
    
    [_yearPicker retrieveCustomPickerViewControllerDidSpinCallback:^(int year)
     {
         _year = _minYear - 1 + year;
        
         NSInteger _month = [self getChosenMonthFromPicker];
         NSCalendar* cal = [NSCalendar currentCalendar];
         [cal setTimeZone:[self getTimeZone]];
         NSDateComponents* comps = [[NSDateComponents alloc] init];
         [comps setMonth:_month];
         
         _year = [self getChosenYearFromPicker];
         [comps setYear:_year];
         [comps setDay:1];
         
         if (currentCalendarStyle == CalendarStyle_Weekly) {
             NSDate *newDate = [cal dateFromComponents:comps];
             _weekPicker.data4Rows = [self getFullListOfData:newDate];
             [_weekPicker setDataIndex:0];
         }
         else if (currentCalendarStyle == CalendarStyle_Daily){
             _dayPicker.data4Rows = [self getDaysStructArrayFor:comps];
         }
         [self getChosenDateFromPicker];
     }];
}


-(void)monthPickerConfiguration:(CGRect)monthPkrFrame
{
    // Month Picker
    _mounthPicker = [[CustomPickerView alloc] initWithFrame:CGRectZero background:_monthImage itemVerticalOffset:0.0f andData:[self getMonthPickerData]];
    _mounthPicker.delegate = self;
    _mounthPicker.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_mounthPicker retrieveCustomPickerViewControllerDidSpinCallback:^(int month)
     {
         
         NSCalendar* cal = [NSCalendar currentCalendar];
         
         NSDateComponents* comps = [[NSDateComponents alloc] init];
         [comps setMonth:month];
         [comps setYear:_year];
         
         if (currentCalendarStyle == CalendarStyle_Daily) {
             
             _year = [self getChosenYearFromPicker];
             [comps setYear:_year];
             [comps setMonth:month];
             [comps setDay:1];
             
            _dayPicker.data4Rows = [self getDaysStructArrayFor:comps];
         }
         
         if (currentCalendarStyle == CalendarStyle_Weekly)
         {
             _year = [self getChosenYearFromPicker];
             [comps setYear:_year];
             [comps setMonth:month];
             [comps setDay:2];
             
             NSDate *newDate = [cal dateFromComponents:comps];
             _weekPicker.data4Rows = [self getFullListOfData:newDate];
             [_weekPicker setDataIndex:0];
             _weekPicker.selectedIndex = 0;
             
         }
         
         [self getChosenDateFromPicker];
         
     }];
}

-(void)dayPickerConfiguration:(CGRect)dayPkrFrame
{
    //Day Picker
    _dayPicker = [[CustomPickerView alloc] initWithFrame:CGRectZero background:_dayImage itemVerticalOffset:0.0f andData:[self getDayPickerData]];
    _dayPicker.delegate = self;
    _dayPicker.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_dayPicker retrieveCustomPickerViewControllerDidSpinCallback:^(int day)
     {
         [self getChosenDateFromPicker];
     }];
}

-(void)weekPickerConfiguration:(CGRect)weekPkrFrame
{
    _weekPicker = [[CustomPickerView alloc] initWithFrame:weekPkrFrame background:_weekImage itemVerticalOffset:0.0f andData:[self getWeeklyPickerData]];
    _weekPicker.delegate = self;
    _weekPicker.translatesAutoresizingMaskIntoConstraints = NO;
    [_weekPicker retrieveCustomPickerViewControllerDidSpinCallback:^(int day)
     {
         _weekPicker.selectedIndex=day-1;
         [self getChosenDateFromPicker];
     }];
}


-(void)getChosenDateFromPicker
{
    NSString *yearString = [NSString string];
    if ( _yearPicker.selectedIndex < [_yearPicker.data4Rows count]) {
        
    }
    else {
        _yearPicker.selectedIndex = 0;
    }
    DateDataStructure *yearStruct = _yearPicker.data4Rows[_yearPicker.selectedIndex];
    yearString = yearStruct.displayString;
    
    NSString *monthString = [NSString string];
    if ( _mounthPicker.selectedIndex < [_mounthPicker.data4Rows count]) {
        
    }
    else {
        _mounthPicker.selectedIndex = 0;
    }
    DateDataStructure *monthStruct = _mounthPicker.data4Rows[_mounthPicker.selectedIndex];
    monthString = monthStruct.displayString;
    
    NSString *dayString = [NSString string];
    if ( _dayPicker.selectedIndex < [_dayPicker.data4Rows count]) {
        
    }
    else {
        _dayPicker.selectedIndex = 0;
    }
    DateDataStructure *dayStruct = _dayPicker.data4Rows[_dayPicker.selectedIndex];
    dayString = dayStruct.displayString;
    
    NSString *dateString = [NSString stringWithFormat:@"%@-%@-%@",dayString,monthString,yearString];
    
    DateDataStructure *dateStruct= [[DateDataStructure alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[self getTimeZone]];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [formatter dateFromString:dateString];
    dateString = [formatter stringFromDate:date];
    
    dateStruct.weekStartDate    = date;
    dateStruct.weekEndDate      = date;
    dateStruct.weekStartString  = dateString;
    dateStruct.weekEndString    = dateString;
    dateStruct.currentCalendarStyle = [self convertToString:currentCalendarStyle];
    
    if (currentCalendarStyle == CalendarStyle_Weekly) {
        int index = (int) ((_weekPicker.selectedIndex < [_weekPicker.data4Rows count])? _weekPicker.selectedIndex:0);
        DateDataStructure *weekStruct = _weekPicker.data4Rows[index];
        dateStruct.weekStartDate    = weekStruct.weekStartDate;
        dateStruct.weekEndDate      = weekStruct.weekEndDate;
        dateStruct.weekStartString  = [formatter stringFromDate:weekStruct.weekStartDate];
        dateStruct.weekEndString    = [formatter stringFromDate:weekStruct.weekEndDate];
        dateStruct.currentCalendarStyle = [self convertToString:currentCalendarStyle];
    }
    else{
        
    }
    [self.delegate datePickerDateChange:self forDate:dateStruct];
}


#pragma mark prepare View
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_maximumDate < _date)
    {
        _date = _maximumDate;
    }

    [self configCalendarComponent];
    
    NSDateComponents *comp = [_calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:_date];
    
    NSInteger year = [comp year];
    NSInteger month = [comp month];
    NSInteger day = [comp day];
    
    [_yearPicker setDataIndex:year - _minYear];
    [_mounthPicker setDataIndex:month - 1];
    [_dayPicker setDataIndex:day - 1];
    
    
    NSDate *currentDate = [NSDate date];
    [self setWeekPickerSelectedIndexByPassingDate:currentDate];
    
    [_yearPicker.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_yearPicker.selectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    [_dayPicker.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_dayPicker.selectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
   
    [_weekPicker.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_weekPicker.selectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    [_mounthPicker.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_mounthPicker.selectedIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    [_yearPicker.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_yearPicker.selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    
    [self getChosenDateFromPicker];
}

-(void)setWeekPickerSelectedIndexByPassingDate:(NSDate *)_chosendate
{
    NSArray *thisMonthWeekArray  =  [self getFullListOfData:_chosendate];
    int i=0;
    
    for (DateDataStructure *thisWeek in thisMonthWeekArray) {
        BOOL isBetween = [self date:_chosendate isBetweenDate:thisWeek.weekStartDate andDate:thisWeek.weekEndDate];
        if (isBetween){
            [_weekPicker setDataIndex:i];
            break;
        }
        i++;
    }
}

-(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

#pragma mark - memory managment

- (void)dealloc
{
    _dayPicker = nil;
    _yearPicker = nil;
    _yearPicker = nil;
    _monthImage = nil;
    _monthImage = nil;
    
    self.dayImage = nil;
    self.yearImage = nil;
    self.monthImage = nil;
    self.calendar = nil;
}

#pragma mark - callback functions

-(void)customDatePickerHasChangedCallBack:(CustomDatePickerChangeCallback)block
{
    self.customDatePickerChangeCallback = block;
}
#pragma mark UICustomPickerConrol Delegate

- (void)pickerControllerDidSpin:(CustomPickerView *)controller;
{

}

- (void)pickerController:(CustomPickerView *)dial didSnapToString:(NSString *)string
{
    if(dial)
    {
        if (_customDatePickerChangeCallback)
        {
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:1+_dayPicker.selectedIndex];
            [comps setMonth:1 + _mounthPicker.selectedIndex];
            [comps setYear:_minYear + _yearPicker.selectedIndex];
            NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
            if (self.customDatePickerChangeCallback)
            {
                self.customDatePickerChangeCallback(date);
            }
        }
    }
    else{
        NSLog(@"Dial is NULL");
    }
    
}

- (NSString*) convertToString:(CalendarStyle) whichAlpha {
    NSString *result = nil;
    
    switch(whichAlpha) {
        case CalendarStyle_Daily:
            result = @"1";
            break;
        case CalendarStyle_Weekly:
            result = @"2";
            break;
        case CalendarStyle_Monthly:
            result = @"3";
            break;
        case CalendarStyle_Yearly:
            result = @"4";
            break;
        default:
            result = @"5";
    }
    
    return result;
}



-(NSArray *)getFullListOfData:(NSDate *)pickerDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitYear) fromDate:pickerDate];
    NSInteger yearInt = [dateComponents year];
    
    
    NSArray *month = [self getMonthListOfYear:(int)yearInt];
    NSMutableArray *weekList = [NSMutableArray array];
    for (NSDate *startMonth in month) {
//        [weekList addObject:[self getWeekListOfMonth:startMonth]];
        [weekList addObjectsFromArray:[self getWeekListOfMonth:startMonth]];
    }
    return [weekList copy];
}


-(NSArray *)getWeekDisplayNameArray:(NSArray *)_inputArray
{
    NSMutableArray *weekArray = [NSMutableArray array];
    for (DateDataStructure *weekStruct in _inputArray) {
        [weekArray addObject:weekStruct.displayString];
    }
    
    return [weekArray copy];
}


-(NSArray *)getMonthListOfYear:(int)year
{
    NSMutableArray *mothlist_ary = [NSMutableArray array];
    
    //NSInteger startingMonth = 1;
    NSInteger startingYear = year;
    
    // we'll need this in several places
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[self getTimeZone]];
    
    // build the first date (in the starting month and year)
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    //[comps setMonth:startingMonth];
    [comps setYear:startingYear];
    // [comps setDay:1];
    NSDate *date = [cal dateFromComponents:comps];
    
    // this is our output format
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDateFormatter *format1 = [[NSDateFormatter alloc] init];
    [format1 setDateFormat:@"MM/dd/yyyy"];
    // we need NSDateComponents for the difference, i.e. at each step we
    // want to go one month further
    NSDateComponents *comps2 = [[NSDateComponents alloc] init];
    [comps2 setMonth:1];
    
    for (int i= 0; i < 12; i++) {
        [mothlist_ary addObject:date];
        // add 1 month to date
        date = [cal dateByAddingComponents:comps2 toDate:date options:0];
    }
    return [mothlist_ary copy];
}


-(NSArray *)getWeekListOfMonth:(NSDate *)pickerDate
{
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setTimeZone:[self getTimeZone]];
    formatter1.dateFormat = @"MM/dd/yyyy HH:mm:ss";
    NSString *dateAsString = [formatter1 stringFromDate:pickerDate];
    NSDate *newDate = [formatter1 dateFromString:dateAsString];
    
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSRange weekRange = [calender rangeOfUnit:NSCalendarUnitWeekOfYear inUnit:NSCalendarUnitMonth forDate:newDate];
    // NSInteger weeksCount=weekRange.length;
    // NSLog(@"%ld",(long)weeksCount);
    
    formatter1.dateFormat = @"yyyy";
    dateAsString = [formatter1 stringFromDate:newDate];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[self getTimeZone]];
    formatter.dateFormat = @"yyyy";
    NSDate *date = [formatter dateFromString:dateAsString];
    NSString *stringFromDate = @"";
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // Create the start date components
    NSDateComponents *oneDayAgoComponents = [[NSDateComponents alloc] init];
    
    [oneDayAgoComponents setMonth:0];
   // NSUInteger numWeekdays = [calendar maximumRangeOfUnit:NSCalendarUnitWeekday].length;
  //  int w=(int)numWeekdays;
   // NSLog(@"numWeekdays = %d",w);
    
    NSMutableArray *weekArray = [NSMutableArray array];
    for(int currentdateindexpath=(int)(weekRange.location-1);currentdateindexpath<(int)((weekRange.location-1)+weekRange.length);currentdateindexpath++)
    {
        
        DateDataStructure *weekStruct = [[DateDataStructure alloc] init];
        
        [oneDayAgoComponents setWeekOfYear:currentdateindexpath];
        
        NSDate *monthAgo = [calendar dateByAddingComponents:oneDayAgoComponents
                                                     toDate:date
                                                    options:0];
        [formatter setDateFormat:@"MMMM yyyy"];
       // NSString *montYear = [formatter stringFromDate:monthAgo];
        
        [formatter setDateFormat:@"'week'W"];
        stringFromDate = [formatter stringFromDate:monthAgo];
        NSDateComponents* subcomponent = [calendar components:NSCalendarUnitWeekday
                                                     fromDate:date];
        
        
        [oneDayAgoComponents setDay: 0 - ([subcomponent weekday] - 1)];
        //NSLog(@"oneDayAgoComponents1 %@",oneDayAgoComponents);
        NSDate *beginningOfWeek = [calendar dateByAddingComponents:oneDayAgoComponents
                                                            toDate:date options:0];
        
        //NSLog(@" beginningOfWeek %@",beginningOfWeek);
        [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        [formatter setTimeZone:[self getTimeZone]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *weekstartdate = [formatter stringFromDate:beginningOfWeek];
        // NSLog(@"weekstartdate %@",weekstartdate);
        weekStruct.weekStartString = weekstartdate;
        weekStruct.weekStartDate =  [formatter dateFromString:weekstartdate];
        
        NSDateComponents *components =
        [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |
                              NSCalendarUnitDay) fromDate: beginningOfWeek];
        
        beginningOfWeek = [calendar dateFromComponents:components];
        /*
         for(int d=0;d<=w;d++){
         NSDate *newDate1 = [beginningOfWeek dateByAddingTimeInterval:60*60*24*d];
         NSString *date_ = [formatter stringFromDate:newDate1];
         // NSLog(@"days %@",date_);
         //NSLog(@"Show %@",beginningOfWeek);
         }*/
        [oneDayAgoComponents setDay:7- ([subcomponent weekday])];
        
      //  NSLog(@"oneDayAgoComponents2 %@",oneDayAgoComponents);
        NSDate *endOfWeek = [calendar dateByAddingComponents:oneDayAgoComponents
                                                      toDate:date options:0];
      //  NSLog(@" endOfWeek %@",endOfWeek);
        [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        NSString *weekendtdate = [formatter stringFromDate:endOfWeek];
      //  NSLog(@"weekendtdate %@",weekendtdate);
        
        weekStruct.weekEndDate = [formatter dateFromString:weekendtdate];
        weekStruct.weekEndString = weekendtdate;
        
        [formatter setDateFormat:@"MMM dd"];
        weekStruct.displayString = [NSString stringWithFormat:@"%@ - %@",[formatter stringFromDate:weekStruct.weekStartDate],[formatter stringFromDate:weekStruct.weekEndDate] ];
        
        [weekArray addObject:weekStruct];
       // NSLog(@"...>%@",[NSString stringWithFormat:@"%@ %@ to%@   to %@ ",stringFromDate,weekstartdate,weekendtdate,monthAgo]);
    }
    
    return [weekArray copy];
}



@end
