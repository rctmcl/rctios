//
//  NSDate+DateManager.m
//  TRKRLabsiOS
//
//  Created by inexgen on 12/02/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

#import "NSDate+DateManager.h"

@implementation NSDate (DateManager)


- (NSDate *)dateForAllDay {
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    [dateComponents setHour:12];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    
    return [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
}

- (NSDate *)dateTimeAtHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSCalendarUnit dateUnit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *dateComponents = [calendar components:dateUnit
                                                   fromDate:self];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate *)dateOnly {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    [dateComponents setHour:0];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate *)dateByAddingYear:(NSInteger)dYears
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:dYears];
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    
    return newDate;
}

- (NSDate *)dateByAddingMonth:(NSInteger)dMonths
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:dMonths];
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    
    return newDate;
}

- (NSDate *)dateByAddingDay:(NSInteger)dDays
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:dDays];
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    
    return newDate;
}

- (NSDate *)dateByAddingHour:(NSInteger)dHours
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setHour:dHours];
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    
    return newDate;
}

- (NSDate *)dateByAddingMinute:(NSInteger)dMinutes
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMinute:dMinutes];
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    
    return newDate;
}

- (NSInteger)hour {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitHour) fromDate:self];
    
    return [dateComponents hour];
}

- (NSInteger)minute {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitMinute) fromDate:self];
    
    return [dateComponents minute];
}

- (NSInteger)year {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitYear) fromDate:self];
    
    return [dateComponents year];
}

- (NSInteger)weekday {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitWeekday) fromDate:self];
    
    return [dateComponents weekday];
}

- (NSInteger)weekNumber {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitWeekOfYear) fromDate:self];
    
    return [dateComponents weekOfYear];
}

- (NSDate *)startOfWeek {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDate *startOfWeek = nil;
    BOOL ok = [calendar rangeOfUnit:NSCalendarUnitWeekday startDate:&startOfWeek
                           interval:NULL forDate:self];
    if (ok) {
        return startOfWeek;
    }
    
    // couldn't calc via range, so try to grab Sunday, assuming gregorian style
    // Get the weekday component of the current date
    NSDateComponents *weekdayComponents = [calendar components:NSCalendarUnitWeekday fromDate:self];
    
    /*
     Create a date components to represent the number of days to subtract from the current date.
     The weekday value for Sunday in the Gregorian calendar is 1, so subtract 1 from the number of days to subtract from the date in question.  (If today's Sunday, subtract 0 days.) */
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    
    [componentsToSubtract setDay: 0 - ([weekdayComponents weekday] - [calendar firstWeekday])];
    startOfWeek = [calendar dateByAddingComponents:componentsToSubtract toDate:self options:0];
    
    //normalize to midnight, extract the year, month, and day components and create a new date from those components.
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:startOfWeek];
    return [calendar dateFromComponents:components];
}

- (NSDate *)endOfWeek {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *weekdayComponents = [calendar components:NSCalendarUnitWeekday fromDate:self];
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    [componentsToAdd setDay:(7 - [weekdayComponents weekday])];
    NSDate *endOfWeek = [calendar dateByAddingComponents:componentsToAdd toDate:self options:0];
    
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:endOfWeek];
    return [calendar dateFromComponents:components];
}

- (NSDate *)startOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:self];
    
    return [calendar dateFromComponents:components];
}

- (NSDate *)endOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSRange daysRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self];
    
    NSDateComponents *comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                          fromDate:self];
    [comps setDay:daysRange.length];
    
    return [calendar dateFromComponents:comps];
}

- (NSDate *)startOfYear {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitYear fromDate:self];
    
    return [calendar dateFromComponents:components];
}

- (NSDate *)endOfYear {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                          fromDate:self];;
    [comps setMonth:12];
    
    return [[calendar dateFromComponents:comps] endOfMonth];
}

- (NSUInteger)numberOfWeeksForMonthOfDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDate *firstDayInMonth = [calendar dateFromComponents:[calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:self]];
    
    NSDate *lastDayInMonth = [calendar dateByAddingComponents:((^{
        NSDateComponents *dateComponents = [NSDateComponents new];
        dateComponents.month = 1;
        dateComponents.day = -1;
        return dateComponents;
    })()) toDate:firstDayInMonth options:0];
    
    NSDate *fromSunday = [calendar dateFromComponents:((^{
        NSDateComponents *dateComponents = [calendar components:NSCalendarUnitWeekOfYear|NSCalendarUnitYearForWeekOfYear fromDate:firstDayInMonth];
        dateComponents.weekday = 1;
        return dateComponents;
    })())];
    
    NSDate *toSunday = [calendar dateFromComponents:((^{
        NSDateComponents *dateComponents = [calendar components:NSCalendarUnitWeekOfYear|NSCalendarUnitYearForWeekOfYear fromDate:lastDayInMonth];
        dateComponents.weekday = 1;
        return dateComponents;
    })())];
    
    return 1 + [calendar components:NSCalendarUnitWeekOfYear fromDate:fromSunday toDate:toSunday options:0].weekOfYear;
}


@end
