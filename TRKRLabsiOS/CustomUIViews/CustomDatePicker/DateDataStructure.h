//
//  DateDataStructure.h
//  CustomDatePickerViewArc
//
//  Created by inexgen on 18/12/15.
//  Copyright © 2015 Z. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,CalendarStyle)
{
    CalendarStyle_Daily,
    CalendarStyle_Weekly,
    CalendarStyle_Monthly,
    CalendarStyle_Yearly
};


@interface DateDataStructure : NSObject
@property(nonatomic,strong) NSDate *weekStartDate;
@property(nonatomic,strong) NSDate *weekEndDate;
@property(nonatomic,strong) NSString *weekStartString;
@property(nonatomic,strong) NSString *weekEndString;
@property(nonatomic,strong) NSString *displayString;
@property(nonatomic,strong) NSString *currentCalendarStyle;
@end
