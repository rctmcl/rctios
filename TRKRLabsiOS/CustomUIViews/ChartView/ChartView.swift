//
//  ChartView.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 15/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation
import CoreGraphics;
import UIKit;
import Charts
import ChameleonFramework
//import TRKRLabsiOSswift

protocol ChartViewGetUserProfileDelegate{
    
    func getUserProfile()->UserProfile?
    func chartViewResponseErrorBlock(error:ErrorResponse?)->Void
    
}

class ChartView : UIView,ChartViewDelegate,CustomDatePickerDelegate
{
    @IBOutlet var view : UIView?
    @IBOutlet var chartView : LineChartView?
    @IBOutlet var cDatePicker : CustomDatePicker?
    @IBOutlet var segmentCtrl: UISegmentedControl?
    @IBOutlet var sleepCircle : CustomCircleProgressView?
    @IBOutlet var staminaCircle : CustomCircleProgressView?
    
    var selectedUserProfile : FanDetails?
    let athleteStatsManager = AthleteStatsManager()
    var athleteStatsData: [AthleteStatsData]?
    
    var userProfileDelegate : ChartViewGetUserProfileDelegate?
    var chartDefaultData : [String:String] = [
        "0" : "0",
        "1" : "0",
    ]
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    
    private func setUp()
    {
        NSBundle.mainBundle().loadNibNamed("ChartView", owner: self, options: nil)
        self.addSubview(self.view!);

        cDatePicker?.delegate = self;
        chartView?.delegate = self
        chartView?.xAxis.labelPosition = .Bottom
        chartView?.xAxis.labelTextColor = UIColor.whiteColor()
      
        chartView?.noDataText = ""
        chartView?.descriptionText = ""
        chartView?.noDataTextDescription = ""//"You need to provide data for the stats"
        
        chartView?.dragEnabled = true
        chartView?.setScaleEnabled(true)
        chartView?.pinchZoomEnabled = true
        chartView?.drawGridBackgroundEnabled = true
        chartView?.gridBackgroundColor = UIColor.clearColor()

        let leftAxis:ChartYAxis = (chartView?.leftAxis)!
        leftAxis.gridColor = UIColor.whiteColor()
        leftAxis.inverted = false
        
        leftAxis.valueFormatter = NSNumberFormatter()
        leftAxis.valueFormatter!.minimumIntegerDigits = 1
        leftAxis.valueFormatter!.maximumFractionDigits = 1
        leftAxis.valueFormatter!.minimumFractionDigits = 1
        leftAxis.valueFormatter!.usesGroupingSeparator = true
        
       // leftAxis.entries = [0.0,50.0,60.0,70.0,80.0,90.0,100.0]
        leftAxis.labelCount = 6
        leftAxis.valueFormatter?.positiveSuffix = " %"
        leftAxis.valueFormatter?.negativePrefix = " %"
        leftAxis.customAxisMax = 100.0;
        leftAxis.customAxisMin = 40.0;
      //  leftAxis.forceLabelsEnabled = true
        leftAxis.startAtZeroEnabled = false
        leftAxis.labelTextColor = UIColor.whiteColor()
   
        let marker = BalloonMarker(color: UIColor.whiteColor(), font: UIFont.systemFontOfSize(12.0), insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0))
        marker.minimumSize = CGSizeMake(80.0, 4.0)
        chartView?.marker = marker
        chartView?.rightAxis.enabled = false
        
        chartView?.viewPortHandler.setMaximumScaleX(1.5)
        chartView?.viewPortHandler.setMaximumScaleY(1.0)
        
        chartView?.legend.enabled = false

        fillTheDummyData()
        // Animate Line while landing page
        chartView?.animate(xAxisDuration:0, easingOption:ChartEasingOption.EaseInOutQuart)
    }
    
    
    func fillTheDummyData()
    {
        let xValues = Array(chartDefaultData.keys)
        let yValues = Array(chartDefaultData.values)
        
        chartDataSource(xValues, yValues: yValues)
    }
    
    
    func chartDataSource(var xLabels:[String],var yValues:[String])
    {
        xLabels.insert("0", atIndex: 0)
        yValues.insert("40", atIndex: 0)
        
        var xVals = [String?]()
        for svalue in xLabels {
            xVals.append(svalue)
        }
        
        var yVals = [ChartDataEntry]()
     /*   for _ in 0...11 {
            let mult = UInt32(65535)
            let val:Double = Double( arc4random_uniform(mult))
            print(val)
           // let tElement = ChartDataEntry(value: val, xIndex: index)
           // yVals.append(tElement)
        }
       */
        
     /*   for index in 0...9 {
            print("index = \(index)")
            let tElement = ChartDataEntry(value: Double(index*10), xIndex: index)
            yVals.append(tElement)
        } */
        
        for (index,sValue) in yValues.enumerate() {
            var xValue = "40"
            if Int(sValue) <= 40 {
                xValue = "40"
            }
            else {
                 xValue = sValue
            }
            let tElement = ChartDataEntry(value: Double(xValue)!, xIndex: index)
                
            yVals.append(tElement)
        }

        let set1:LineChartDataSet = LineChartDataSet(yVals: yVals)
        set1.highlightLineDashLengths = [5.0,2.5]
        set1.setColor(UIColor.greenColor())
        set1.setCircleColor(UIColor.greenColor())
        set1.lineWidth = 2.0
        set1.circleRadius = 3.0
        set1.drawCircleHoleEnabled = false
        set1.valueFont = UIFont.systemFontOfSize(9.0)
        set1.fillAlpha = 65/255.0
        set1.fillColor = UIColor.blackColor()

        
        var dataSets = [LineChartDataSet]()
        dataSets.append(set1)
        
        
        let data : LineChartData = LineChartData(xVals: xVals, dataSets: dataSets)
        chartView?.data = data
        
        for set in chartView!.data!.dataSets
        {
            set.drawValuesEnabled = !set.isDrawValuesEnabled;
        }
        
        chartView?.notifyDataSetChanged()
        chartView?.setNeedsDisplay()
        chartView?.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
    }
    
    
// MARK: Segment Control
    @IBAction func segmentCtrlValueChanged(sender : AnyObject)
    {
        let segCtrl:UISegmentedControl = sender as! UISegmentedControl
        
        if (segCtrl.selectedSegmentIndex == 0) {
            cDatePicker!.currentCalendarStyle = .Daily;
        }
        else if (segCtrl.selectedSegmentIndex == 1){
            cDatePicker!.currentCalendarStyle = .Weekly;
        }else if(segCtrl.selectedSegmentIndex == 2){
            cDatePicker!.currentCalendarStyle = .Monthly;
        }
        else{
            cDatePicker!.currentCalendarStyle = .Yearly;
        }
        
        cDatePicker!.configCalendarComponent()
    }
    
    func datePickerDateChange(piker: CustomDatePicker!, forDate date: DateDataStructure!) {
        pickerDateChange(piker, forDate: date)
    }
    
    
    func setSleepPercentageData(percentageValue:String)
    {
        if percentageValue.isEmpty
        {
             sleepCircle?.setDountValues("0", aRecord: AthleteRecord.SleepQuality )
        }
        else
        {
             sleepCircle?.setDountValues(percentageValue, aRecord: AthleteRecord.SleepQuality )
        }
    }
   
    func setEffortPercentageData(percentageValue:String)
    {
        if percentageValue.isEmpty
        {
            staminaCircle?.setDountValues("0", aRecord: AthleteRecord.Effort )
        }
        else
        {
            staminaCircle?.setDountValues(percentageValue, aRecord: AthleteRecord.Effort )
        }
    }
    
    
    func getAthleteWeeklyData(currentUser : AthleteUserProfile?) -> Void
    {
        athleteStatsManager.getAthleteData(currentUser!, callback: { (response:AthleteStats) -> () in
            self.parseResponse(response)
            }) { (error:ErrorResponse) -> () in
                self.userProfileDelegate?.chartViewResponseErrorBlock(error)
        }
    }
    
    func parseResponse(response:AthleteStats)
    {
        if response.result != nil{
            athleteStatsData = response.result
        }
        else{
            athleteStatsData?.removeAll()
        }
        parse(athleteStatsData)
    }
    
    func parse(athleteData : [AthleteStatsData]?)
    {
        var xValues : [String] = [String]()
        var yDatas  : [String] = [String]()
        var staminaArray : [String] = [String]()
        var sleepArray   : [String] = [String]()
        
       // xValues.append("0")
       // yDatas.append("0")
        
        
        if (athleteData != nil ) && (athleteData?.count > 0 )
        {
            for asData in athleteData!
            {
                if CalendarStyle.Yearly == getCalendarStyleForType(asData.type!)
                {
                    xValues.append(getMonthNameFromDigit(asData.month_no!))
                    yDatas.append(asData.effort_percent!)
                }
                else{
                    xValues.append(getDayFromDate(asData.ess_date!))
                    yDatas.append(asData.effort_percent!)
                }
                
                if asData.sleep_percent != nil {
                    sleepArray.append(asData.sleep_percent!)
                }
                
                if asData.stamina_percent != nil {
                    staminaArray.append(asData.stamina_percent!)
                }
            }
        }
        else {
            // Empty Data
            chartView?.noDataText="No data available"
        }
    
        //Pass X & Y Values to Chart
        chartDataSource(xValues,yValues: yDatas)
        
        setSleepPercentageData(getPercentageValueForEntity(sleepArray))
        setEffortPercentageData(getPercentageValueForEntity(yDatas))
        
    }
    
    
    func getPercentageValueForEntity(entityArray:[String])->String
    {
        var totalEntity : Float = 0
        
        if entityArray.count == 0 {
            return "0";
        }
        
        var arrayCount : Int = 0
        for entityData in entityArray {
            let intValue = Float(entityData)
            if intValue < 40.0 {
                continue
            }
            arrayCount = arrayCount + 1
            totalEntity = totalEntity + intValue!
        }
        if arrayCount == 0 {
            return "0"
        }
        totalEntity = totalEntity / Float(arrayCount)
        return String(Int(round(totalEntity)))
    }
    
    
    func getDayFromDate(dateString : String)->String
    {
        if dateString.characters.count > 0 {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.dateFromString(dateString)
            dateFormatter.dateFormat = "dd"
            let dayString = dateFormatter.stringFromDate(date!)
            return dayString
        }else{
            return "0"
        }
    }
    
    func getMonthNameFromDigit(monthInDigit : String)->String{
        let months:[String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"];
        let monthInInt = Int(monthInDigit)
        return months[(monthInInt!-1)]
    }
    
    //MARK : ChartView Date Picker Delegate
    func pickerDateChange(piker: CustomDatePicker!, forDate date: DateDataStructure!) {
        
        guard let currentUser = userProfileDelegate?.getUserProfile() else
        {
            return
        }
        
        guard let tdate = date else{
            return
        }
        
        let athleteUser =  AthleteUserProfile()
        athleteUser.user_id = currentUser.user_id
        athleteUser.token = currentUser.token
        athleteUser.startdate = tdate.weekStartString
        athleteUser.enddate = tdate.weekEndString
        athleteUser.type = tdate.currentCalendarStyle
        
        getAthleteWeeklyData(athleteUser)
    }
    
    
    func getCalendarStyleForType(calendarType : String)->CalendarStyle
    {
        switch(calendarType)
        {
        case "1" :
            return CalendarStyle.Daily
            // break
        case "2" :
            return CalendarStyle.Weekly
            //break
        case "3" :
            return CalendarStyle.Monthly
            //break
        case "4":
            return CalendarStyle.Yearly
        default:
            return CalendarStyle.Daily
            
        }
    }

}