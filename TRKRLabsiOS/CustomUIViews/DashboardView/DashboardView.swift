//
//  DashboardView.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 15/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import Device_swift

class DashboardView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var dashBoardCollectionView: UICollectionView!
    @IBOutlet var athleteStatisticsCollectionView: UICollectionView!
    @IBOutlet var athleteProfileImageView: UIImageView!
    @IBOutlet var athleteNameLabel: UILabel!
    @IBOutlet var athleteWeightLabel: UILabel!
    @IBOutlet var athleteHeightLabel: UILabel!
    
    
    var dashboardData:DashboardData?
    
    
    let SEDENTARY_MINS_COLOR = UIColor(red: 86/255.0, green: 178/255.0, blue: 226/255.0, alpha: 1.0)
    let ACTIVE_MINS_COLOR = UIColor(red: 87/255.0, green: 173/255.0, blue:74/255.0, alpha: 1.0)
    let HR_REST_MINS_COLOR = UIColor(red: 211/255.0, green: 80/255.0, blue: 85/255.0, alpha: 1.0)
    let HR_ACTVIE_MINS_COLOR = UIColor(red:127/255.0, green: 61/255.0, blue: 63/255.0, alpha: 1.0)
    
   //MARK : INIT METHOD
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
        
    }
     //MARK : Decoder Init METHOD
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    private func commonInit() {
    
        NSBundle.mainBundle().loadNibNamed("DashboardView", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        self.addSubview(content)
        dashBoardCollectionView.registerNib(UINib(nibName: "DonutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: DONUT_COLLECTION_CELL_ID)
         athleteStatisticsCollectionView.registerNib(UINib(nibName: "AthleteRecordCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ATHLETE_RECORD_COLLECTION_CELL_ID)
        
        
    }

    //MARK: set Athlete Dashboard Values
    func setDashboardDataValues(dbData:DashboardData) -> Void
    {
        dashboardData = dbData
        if let athleteData = dashboardData
        {
            athleteNameLabel.text =   athleteData.user_name?.capitalizeFirst
            if let weight = athleteData.user_weight
            {
                athleteWeightLabel.text =  weight + " " + TRKR_WEIGHT_UNIT
            }
            else
            {
                  athleteWeightLabel.text = ""
            }
            if let height = athleteData.user_height
            {
                athleteHeightLabel.text = TRKRUtility.getFormattedHeight(height,page: "Dashboard")
            }
            else
            {
                 athleteHeightLabel.text = ""
            }
            
            setProfileImage(athleteData.user_profile_image,imageView: athleteProfileImageView)
            
            dashBoardCollectionView.reloadData()
            athleteStatisticsCollectionView.reloadData()
        }
    }
    
    //MARK:  SetProfileImage
    func setProfileImage(imageUrl : String?,imageView : UIImageView?)
    {
        if imageView != nil {
            TRKRUtility.setProfileImage(imageUrl, imageView: imageView!)
        }
    }
    

}




//MARK : UIcollection view Delegate & Datasource

extension DashboardView:UICollectionViewDelegate,UICollectionViewDataSource
{
    //MARK:UIColloectionView Methods
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == dashBoardCollectionView
        {
            return 3
        }
        else
        {
            return 4
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if collectionView == dashBoardCollectionView
        {
            let cell:DonutCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(DONUT_COLLECTION_CELL_ID, forIndexPath: indexPath) as! DonutCollectionViewCell
            setAthleteRecordForRowAtIndexPath(indexPath, cell: cell)
            return cell
        }
        else
        {
                let cell:AthleteRecordCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(ATHLETE_RECORD_COLLECTION_CELL_ID, forIndexPath: indexPath) as! AthleteRecordCollectionViewCell
                setAthleteStatsForRowAtIndexPath(indexPath,cell: cell)
                return cell
        }
    }
    
    
    func setAthleteStatsForRowAtIndexPath(indexPath:NSIndexPath,cell:AthleteRecordCollectionViewCell) -> Void
    {
        guard let _ = dashboardData else
        {
            setAthleteDefaultStatsForRowAtIndexPath(indexPath,cell:cell)
            return
        }
        if indexPath.row == 0
        {
            cell.statsIconImageView.image = UIImage(named: "SedentaryMinsIconImage")
            cell.statsTitleLabel.text = "Sedentary Mins"
            
            if let sedentary_minutes = dashboardData?.sedentary_minutes
            {
                cell.statsValueLabel.text = sedentary_minutes + " " + TRKR_MINUTES_UNIT
            }
            else
            {
                cell.statsValueLabel.text = "0" + " " + TRKR_MINUTES_UNIT
            }
            
            cell.statsTitleLabel.textColor = SEDENTARY_MINS_COLOR
        }
        else if indexPath.row == 1
        {
            cell.statsIconImageView.image = UIImage(named: "ActiveMinsIconImage")
            cell.statsTitleLabel.text = "Active Mins"
            
            if let active_minutes = dashboardData?.active_minutes
            {
                cell.statsValueLabel.text = active_minutes + " " + TRKR_MINUTES_UNIT
            }
            else
            {
                cell.statsValueLabel.text = "0" + " " + TRKR_MINUTES_UNIT
            }
            
             cell.statsTitleLabel.textColor = ACTIVE_MINS_COLOR
        }
        else if indexPath.row == 2
        {
            cell.statsIconImageView.image = UIImage(named: "HeartRateRestingIconImage")
            cell.statsTitleLabel.text = "HR:Resting"
            
            if let resting_heart_rate = dashboardData?.resting_heart_rate
            {
                cell.statsValueLabel.text = resting_heart_rate + " " + TRKR_HEART_BEAT_UNIT
            }
            else{
                cell.statsValueLabel.text = "0" + " " + TRKR_HEART_BEAT_UNIT
            }
             cell.statsTitleLabel.textColor = HR_REST_MINS_COLOR
        }
        else if indexPath.row == 3
        {
            cell.statsIconImageView.image = UIImage(named: "HeartRatePeakMinsIconImage")
            cell.statsTitleLabel.text = "HR:Peak Mins"
            
            if let peak_minutes = dashboardData?.peak_minutes
            {
                cell.statsValueLabel.text = peak_minutes + " " + TRKR_MINUTES_UNIT
            }
            else
            {
                 cell.statsValueLabel.text = "0" + " " + TRKR_MINUTES_UNIT
            }
            
             cell.statsTitleLabel.textColor = HR_REST_MINS_COLOR
        }
    }
    
    func setAthleteRecordForRowAtIndexPath(indexPath:NSIndexPath,cell:DonutCollectionViewCell) -> Void
    {
        guard let _ = dashboardData else
        {
            setAthleteDefaultRecordForRowAtIndexPath(indexPath, cell: cell)
            return
        }
        if indexPath.row == 0
        {
            if let effort = dashboardData?.effort_percent
            {
                cell.circleProgressView.setDountValues(effort ,aRecord: AthleteRecord.Effort)
            }
            else
            {
                cell.circleProgressView.setDountValues("0" ,aRecord: AthleteRecord.Effort)
            }
            
        }
        else if indexPath.row == 1
        {
            if let stamina = dashboardData?.stamina_percent
            {
                cell.circleProgressView.setDountValues(stamina ,aRecord: AthleteRecord.Stamina)
            }
            else
            {
                cell.circleProgressView.setDountValues("0",aRecord: AthleteRecord.Stamina)
            }
        }
        else if indexPath.row == 2
        {
            
            if let sleepQuality = dashboardData?.sleep_percent
            {
                cell.circleProgressView.setDountValues(sleepQuality ,aRecord: AthleteRecord.SleepQuality)
            }
            else
            {
                cell.circleProgressView.setDountValues("0",aRecord: AthleteRecord.SleepQuality)
            }
        }

    }
    
    
    
    func setAthleteDefaultRecordForRowAtIndexPath(indexPath:NSIndexPath,cell:DonutCollectionViewCell) -> Void
    {
        
        if indexPath.row == 0
        {
            cell.circleProgressView.setDountValues("0",aRecord: AthleteRecord.Effort)
            
        }
        else if indexPath.row == 1
        {
            cell.circleProgressView.setDountValues("0" ,aRecord: AthleteRecord.Stamina)
        }
        else if indexPath.row == 2
        {
            cell.circleProgressView.setDountValues("0" ,aRecord: AthleteRecord.SleepQuality)
        }
        
    }
    
    //Empty Values if Dashboard Data is nil
    
    func setAthleteDefaultStatsForRowAtIndexPath(indexPath:NSIndexPath,cell:AthleteRecordCollectionViewCell) -> Void
    {
        if indexPath.row == 0
        {
            cell.statsIconImageView.image = UIImage(named: "SedentaryMinsIconImage")
            cell.statsTitleLabel.text = "Sedentary Mins"
            cell.statsValueLabel.text = "0" + " " + TRKR_MINUTES_UNIT
            
            cell.statsTitleLabel.textColor = SEDENTARY_MINS_COLOR
        }
        else if indexPath.row == 1
        {
            cell.statsIconImageView.image = UIImage(named: "ActiveMinsIconImage")
            cell.statsTitleLabel.text = "Active Mins"
            cell.statsValueLabel.text = "0" + " " + TRKR_MINUTES_UNIT
            
            cell.statsTitleLabel.textColor = ACTIVE_MINS_COLOR
        }
        else if indexPath.row == 2
        {
            cell.statsIconImageView.image = UIImage(named: "HeartRateRestingIconImage")
            cell.statsTitleLabel.text = "HR:Resting"
            cell.statsValueLabel.text = "0" + " " + TRKR_HEART_BEAT_UNIT
            
            cell.statsTitleLabel.textColor = HR_REST_MINS_COLOR
        }
        else if indexPath.row == 3
        {
            cell.statsIconImageView.image = UIImage(named: "HeartRatePeakMinsIconImage")
            cell.statsTitleLabel.text = "HR:Peak Mins"
            cell.statsValueLabel.text = "0" + " " + TRKR_MINUTES_UNIT
            
            cell.statsTitleLabel.textColor = HR_REST_MINS_COLOR
        }
    }

    
}

extension DashboardView : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenRect = collectionView.bounds;
        let screenWidth = screenRect.size.width;
        if collectionView == dashBoardCollectionView
        {
            let cellWidth = screenWidth / 3.1; //Replace the divisor with the column count requirement. Make sure to have it in float.
            let size = CGSizeMake(cellWidth  , cellWidth);
            return size
        }
        else
        {
            let deviceType = UIDevice.currentDevice().deviceType
            var cellwidthconst = 2.01
            switch deviceType {
            case .IPhone6SPlus:
                cellwidthconst = 2.1
            default:
                cellwidthconst = 2.01
            }
            
            let cellWidth = screenWidth / CGFloat(cellwidthconst) //Replace the divisor with the column count requirement. Make sure to have it in float.
            let size = CGSizeMake(cellWidth  , 80);
            return size
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
}