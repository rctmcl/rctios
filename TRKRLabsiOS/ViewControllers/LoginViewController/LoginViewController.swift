//
//  LoginViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 11/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import ChameleonFramework
import TSMessages


class LoginViewController: TRKRBaseViewController
{
    var isTextFieldImageSet = false
    @IBOutlet var emailTextField: CustomTextField!
    @IBOutlet var passwordTextField: CustomTextField!
    @IBOutlet var signInButton: UIButton!
    
    let loginManager = LoginManager()
    let fanManager = FansManager()

    
    
    var buttonTopColor = UIColor(red: 112.0/255.0, green: 112.0/255.0, blue: 112.0/255.0, alpha: 1.0)
    var buttonBottomColor = UIColor(red: 99.0/255.0, green: 99.0/255.0, blue: 99.0/255.0, alpha: 1.0)
    
    let keyChainWrapper = KeychainWrapper()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    //MARK: View Life Cycle
    override func viewDidLoad() {

        TRKRUtility.setBGColor(self.view)
        
        signInButton.backgroundColor = UIColor(gradientStyle:UIGradientStyle.TopToBottom, withFrame:signInButton.frame, andColors:[buttonTopColor,buttonBottomColor])
        
        if let navigationController = self.navigationController
        {
            navigationController.navigationBarHidden = true
        }
        if !isTextFieldImageSet
        {
            setSingleLineTextField(emailTextField,image: "EmailIconImage")
            setSingleLineTextField(passwordTextField,image: "PasswordIconImage")
            isTextFieldImageSet = true
        }
       
    }
    
    override func viewWillAppear(animated: Bool) {
     
        
        if let navigationController = self.navigationController
        {
            navigationController.navigationBarHidden = true
        }
        if !isTextFieldImageSet
        {
            setSingleLineTextField(emailTextField,image: "EmailIconImage")
            setSingleLineTextField(passwordTextField,image: "PasswordIconImage")
            isTextFieldImageSet = true
        }

    }
    
    override func viewDidAppear(animated: Bool) {
       super.viewDidAppear(true)
    }
    
    
    
    
    
    //MARK: Sign In Button Action
    
    @IBAction func signInButtonTapped(sender: UIButton)
    {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        let email:String = emailTextField.text!
        let password:String = passwordTextField.text!
        if TRKRUtility.isValidEmail(email)
        {
           if (password.characters.count > 0)
           {
               //make login call
                signInButton.enabled = false
                login(email, password: password)
           }
           else
           {
             //TRKRUtility.showAlertMessage(kAPP_TITLE, message: "Please enter password",viewController: self)
            showNotificationMessageWithTypeOfNotifcation("Please enter password", notificationtype: TSMessageNotificationType.Error)
           }
        }
        else
        {
            //TRKRUtility.showAlertMessage(kAPP_TITLE, message: "Please enter valid email address",viewController: self)
            showNotificationMessageWithTypeOfNotifcation("Please enter valid email address", notificationtype: TSMessageNotificationType.Error)
        }
    }
    
    
    //MARK: UITextField Customization
    func setSingleLineTextField(textField:UITextField,image:String) -> Void
    {
        setTextFieldImage(textField,image:image)
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.whiteColor().CGColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        textField.attributedPlaceholder = NSAttributedString(string:textField.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
    }
    
    func setTextFieldImage(textField:UITextField,image:String) -> Void
    {
        textField.leftViewMode = UITextFieldViewMode.Always
        textField.leftView = UIImageView(image: UIImage(named: image))
        textField.delegate = self
    }
    
    
    //MARK: Back Button Action
    @IBAction func backButtonTapped(sender: UIButton) {
        
        if let navigationController = self.navigationController
        {
            navigationController.popViewControllerAnimated(true)
        }
    }
    
    
    
    
    //MARK: API
    
    func login(email:String,password:String) -> Void
    {
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            loginManager.login(email, password: password, callback: { (response:UserProfileResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                        self.parseResponse(email,password: password,response: response)
            })
            { (error:ErrorResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.signInButton.enabled = true
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            self.signInButton.enabled = true
            showNetworkConnectivityError()
        }
    }
    
    func parseResponse(email:String,password:String,response:UserProfileResponse) -> Void
    {
        self.signInButton.enabled = true
        saveResponse(email,password: password,response: response)
    }
    
    
    func saveResponse(email:String,password:String,response:UserProfileResponse)
    {
        UserProfileManager.sharedInstance.saveUserProfile(email, password: password, userProfile: response.result!)
        if let role = response.result?.user_role
        {
            if role == "1"
            {
                //Logged in User Role - Athlete
                gotoDashboardPage(role)
            }
            else
            {
                //Logged in User Role-  Fan
                gotoMyAthletePage(role)
            }
        }
       
    }
    
    func gotoDashboardPage(role:String) -> Void
    {
        self.navigationController?.pushViewController(appDelegate.getmmDrawerControllerObject(role), animated: true)
    }
    
    func gotoMyAthletePage(role:String) -> Void
    {
        //For Fan module default page is My ayhlete.
         self.navigationController?.pushViewController(appDelegate.getmmDrawerControllerObject(role), animated: true)
    }
    
    //MARK: Forgot Password Action
    
    @IBAction func forgotPasswordAction(sender: UIButton) {
        
        let fpVC = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_FORGOT_PASSWORD_VC)
        if let forgotPasswordVC  = fpVC
        {
            if let navigationController = self.navigationController
            {
                navigationController.pushViewController(forgotPasswordVC,animated: true)
            }
        }
    }
    
    
}


//MARK:UITextFieldDelegate

extension LoginViewController:UITextFieldDelegate
{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
