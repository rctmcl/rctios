//
//  FansListViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 18/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import SWTableViewCell
import TSMessages
enum FollowStatus:String
{
    case Following   = "1"
    case Requsted    = "2"
    case Rejected    = "3"
   
}

class FansListViewController: TRKRBaseViewController {
    
    
    @IBOutlet var fanListTableView: UITableView!
    var lstFans:[FanDetails]?
    var lstFollowAcceptedFans:[FanDetails]?     = [FanDetails]()
    var lstFollowRequestedFans:[FanDetails]?    = [FanDetails]()
    var lstStates:[StateDetail]?
    var currentViewMode:ViewMode?
    var fanManager = FansManager()
    var athleteManager = AthleteManager()
    var currentUserProfile:UserProfile?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCurrentUserProfile()
        
        if currentUserProfile!.user_role == "1"
        {
            currentViewMode = .AthleteViewMode
            setNavigationController(title: NavigationBarTitle.Fans)
        }
        else
        {
            currentViewMode = .FanViewMode
            setNavigationController(title: NavigationBarTitle.MyAthletes)
        }
        
        if let states = UserProfileManager.sharedInstance.statesList
        {
            lstStates = states
        }
        TRKRUtility.setBGColor(self.view)
        TRKRUtility.removeEmptyCellsInTableView(fanListTableView)
        
        //Nib Registeration
        let nib = UINib(nibName: "FansListCell", bundle: nil)
        fanListTableView.registerNib(nib, forCellReuseIdentifier: FANS_LIST_CELL_ID)
        let nib1 = UINib(nibName: "FanFollowRequestCell", bundle: nil)
        fanListTableView.registerNib(nib1, forCellReuseIdentifier: FAN_FOLLOW_REQUEST_CELL)
        
        // TableView automatic height
        fanListTableView.rowHeight = 60
        fanListTableView.estimatedRowHeight = UITableViewAutomaticDimension
       
    }

    
    func setCurrentUserProfile() -> Void{
        
        guard let user =   UserProfileManager.sharedInstance.getUserProfile() else
        {
            currentUserProfile = UserProfileManager.sharedInstance.getSharedUserProfile()
            return
        }
        currentUserProfile = user
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        if currentViewMode == .AthleteViewMode
        {
            getFansList()
        }
        else
        {
            getAthleteList()
            setNavigationRightButton()
        }
         self.view.alpha = 1.0
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }

    
    override func viewWillDisappear(animated: Bool) {
        
        self.view.alpha = 0.7
    }
    
    
    //MARK: API
    
    func getFansList() -> Void
    {
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            fanManager.getFansList(currentUserProfile!, callback: { (response:FansListResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                   self.parseResponse(response)
                })
                { (error:ErrorResponse) -> () in
                     LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    func getAthleteList() -> Void
    {
        getStatesList()
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            athleteManager.getAthleteList(currentUserProfile!, callback: { (response) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                 self.parseResponse(response)
            }) { (error:ErrorResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                     self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    
    

    func parseResponse(response:FansListResponse) -> Void
    {
        guard let _ = response.result else {
            lstFans?.removeAll()
            lstFollowAcceptedFans?.removeAll()
            lstFollowRequestedFans?.removeAll()
            fanListTableView.reloadData()
            return
        }
        
        lstFans = response.result
        
        if lstFans?.count > 0
        {
            if currentViewMode == .AthleteViewMode
            {
                lstFollowAcceptedFans = getFollowAcceptedFans()
                lstFollowRequestedFans = getFollowRequestedFans()
            }
            else
            {
                lstFollowAcceptedFans = getFollowAcceptedFans()
            }
        }
        else {
            setMessageViewForEmptyTableView(fanListTableView)
        }
        fanListTableView.reloadData()
    }
    func getFollowAcceptedFans() -> [FanDetails]?
    {
        let acceptedFans = lstFans!.filter({
            $0.following_status! == FollowStatus.Following.rawValue
        })
        return acceptedFans
    }
    
    func getFollowRequestedFans() -> [FanDetails]?
    {
        let requestedFans = lstFans!.filter({
            $0.following_status! == FollowStatus.Requsted.rawValue
        })
        return requestedFans
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override  func parseStatesResponse(response:StatesListResponse)
    {
        if let states:[StateDetail] = response.result!
        {
            UserProfileManager.sharedInstance.statesList = states
            let appDelegate :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let menuVC:MenuViewController = (appDelegate.mmDrawerController?.leftDrawerViewController as? MenuViewController)!
            menuVC.menuTableView.reloadData()
             fanListTableView.reloadData()
        }
    }
    

}



//MARK: Table View Delegate & Datasource

extension FansListViewController:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard let _ = lstFollowAcceptedFans, _ = lstFollowRequestedFans else
        {
            return 0
        }
        if section == 0
        {
            return lstFollowAcceptedFans!.count
        }
        else
        {
            return lstFollowRequestedFans!.count
        }
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell:FansListCell = tableView.dequeueReusableCellWithIdentifier(FANS_LIST_CELL_ID) as! FansListCell
            
             cell.selectionStyle = .None;
            if indexPath.row < lstFollowAcceptedFans?.count
            {
                let fanDetail:FanDetails =  lstFollowAcceptedFans![indexPath.row]
                cell.rightUtilityButtons = self.rightButtons() as [AnyObject]
                cell.delegate = self;
                setFollowingFanDetails(cell,fanDetail: fanDetail)
            }
            return cell
        }
        else
        {
            let cell:FanFollowRequestCell = tableView.dequeueReusableCellWithIdentifier(FAN_FOLLOW_REQUEST_CELL) as! FanFollowRequestCell
            
             cell.selectionStyle = .None;
            if indexPath.row < lstFollowRequestedFans?.count
            {
                let fanDetail:FanDetails =  lstFollowRequestedFans![indexPath.row]
                cell.followAcceptButton.addTarget(self, action: "followAcceptButtonTapped:", forControlEvents: .TouchUpInside)
                cell.followRejectButton.addTarget(self, action: "followRejectButtonTapped:", forControlEvents: .TouchUpInside)
                cell.followAcceptButton.tag = indexPath.row
                cell.followRejectButton.tag = indexPath.row
                setFollowRequestedFanDetails(cell,fanDetail: fanDetail)
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if currentUserProfile!.user_role == "2"   // Fan Mode
        {
            let fanDashboardViewController : FanDashboardViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_FAN_DASHBOARD) as! FanDashboardViewController
            if indexPath.section == 0
            {
                if indexPath.row < lstFollowAcceptedFans?.count
                {
                    let fanDetail:FanDetails =  lstFollowAcceptedFans![indexPath.row]
                    fanDashboardViewController.setUserDetails(fanDetail)
                }
            }else{
                if indexPath.row < lstFollowRequestedFans?.count
                {
                     let fanDetail:FanDetails =  lstFollowRequestedFans![indexPath.row]
                    fanDashboardViewController.setUserDetails(fanDetail)
                }
            }
            self.navigationController?.pushViewController(fanDashboardViewController, animated: true)
        }
        else{ //Athlete Mode
            
            let settingsViewController : SettingsViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_SETTINGS_VC) as! SettingsViewController
            if indexPath.section == 0
            {
                if indexPath.row < lstFollowAcceptedFans?.count
                {
                    let fanDetail:FanDetails =  lstFollowAcceptedFans![indexPath.row]
                    settingsViewController.setUserDetailsFromOtherViewController(fanDetail)
                }
            }else{
                if indexPath.row < lstFollowRequestedFans?.count
                {
                    let fanDetail:FanDetails =  lstFollowRequestedFans![indexPath.row]
                    settingsViewController.setUserDetailsFromOtherViewController(fanDetail)
                }
            }
            self.navigationController?.pushViewController(settingsViewController, animated: true)
        }
    }
    
  
    
    
    func setFollowingFanDetails(cell:FansListCell,fanDetail:FanDetails)
    {
        if let name = fanDetail.user_name
        {
            cell.fanNameLabel.text = name
        }
        else
        {
            cell.fanNameLabel.text = ""
        }
        if let stateID = fanDetail.state_id ,city = fanDetail.city
        {
            let  state = TRKRUtility.getStateShortName(stateID)
            if state != ""
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(city) + ", " + state
            }
            else
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(city)
            }
        }
        else
        {
            if fanDetail.city != nil
            {
                 cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(fanDetail.city!)
            }
            else
            {
                cell.fanLocationIcon.hidden = true
            }
        }
        setProfileImage(fanDetail.profile_image, imageView: cell.profileImageView)
    }
    
    func setFollowRequestedFanDetails(cell:FanFollowRequestCell,fanDetail:FanDetails)
    {
        if let name = fanDetail.user_name
        {
            cell.fanNameLabel.text = name
        }
        else
        {
            cell.fanNameLabel.text = ""
        }
        if let stateID = fanDetail.state_id ,city = fanDetail.city
        {
            let  state = TRKRUtility.getStateShortName(stateID)
            if state != ""
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(city) + ", " + state
            }
            else
            {
                 cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(city)
            }
        }
        else
        {
            if fanDetail.city != nil
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(fanDetail.city!)
            }
            else
            {
                cell.fanLocationIcon.hidden = true
            }
        }
        setProfileImage(fanDetail.profile_image, imageView: cell.fanProfileImageView)
    }
    
    func setProfileImage(imageUrl : String?,imageView : UIImageView?)
    {
        if imageView != nil {
            TRKRUtility.setProfileImage(imageUrl, imageView: imageView!)
        }
    }
    
    // MARK: Follow Accept & Reject
    func followAcceptButtonTapped(sender:UIButton) -> Void
    {
        LogManager.sharedInstance.DLog(sender.tag)
        
        let fanDetail:FanDetails = lstFollowRequestedFans![sender.tag]
        fanDetail.following_status = FollowStatus.Following.rawValue
        followRequestUpdate(fanDetail)
        
    }
    func followRejectButtonTapped(sender:UIButton) -> Void
    {
         LogManager.sharedInstance.DLog(sender.tag)
        let fanDetail:FanDetails = lstFollowRequestedFans![sender.tag]
        fanDetail.following_status = FollowStatus.Rejected.rawValue
        followRequestUpdate(fanDetail)
    }
    
    
    //MARK : Goto Fan Oneshot Dashboard
    func gotoDashboard() -> Void
    {
        //Dashboard Page
        let dashboardViewController : DashboardViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_DASHBOARD_VC) as! DashboardViewController
        self.navigationController?.pushViewController(dashboardViewController, animated: true)
    }

    
    //MARK : Follow / Unfollow / Delete API
    func followRequestUpdate(followingFanDetatils:FanDetails) -> Void
    {
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            athleteManager.followRequestUpdate(currentUserProfile!, followingFanDetails: followingFanDetatils, callback: { (response:SuccessResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                self.parseFollowUpdateResponse(response,followingFanDetatils:followingFanDetatils)
                
                })
                { (error:ErrorResponse) -> () in
                    LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    func parseFollowUpdateResponse(response:SuccessResponse,followingFanDetatils:FanDetails) -> Void
    {
        var message = ""
        if currentViewMode == .AthleteViewMode
        {
            message = "Fan"
        }
        else
        {
            message = "Athlete"
        }
        if followingFanDetatils.following_status == FollowStatus.Following.rawValue
        {
            message += " Followed successfully!"
        }
        else if followingFanDetatils.following_status == FollowStatus.Rejected.rawValue
        {
            message += " UnFollowed/Discontinued successfully!"
        }
        //TRKRUtility.showAlertMessage(kAPP_TITLE, message: message, viewController: self)
        showNotificationMessageWithTypeOfNotifcation(message, notificationtype: TSMessageNotificationType.Success)
        self.viewWillAppear(true)
    }
}

//MARK: TableView Cell delegate
extension FansListViewController:SWTableViewCellDelegate
{
    
    func rightButtons() -> NSMutableArray
    {
        let buttons:NSMutableArray = NSMutableArray()
        let title:NSMutableAttributedString = NSMutableAttributedString(string: "Delete")
        title.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor() , range:NSRange(location: 0, length: 6))
        buttons.sw_addUtilityButtonWithColor(UIColor(red: 1.0, green: 0.231, blue: 0.188, alpha: 1.0), attributedTitle:title)
        return buttons
    }
    
    
    func swipeableTableViewCell(cell: SWTableViewCell!, didTriggerRightUtilityButtonWithIndex index: Int)
    {
        let cellIndexPath:NSIndexPath = self.fanListTableView.indexPathForCell(cell)!
        let fanDetail:FanDetails = lstFollowAcceptedFans![cellIndexPath.row]
        fanDetail.following_status = FollowStatus.Rejected.rawValue
        followRequestUpdate(fanDetail)
        LogManager.sharedInstance.DLog(fanDetail.user_name)
        
    }
    
}

extension FansListViewController {
    
    func setMessageViewForEmptyTableView(tableView : UITableView)
    {
        let messageLabel = UILabel()
        //set the message
        messageLabel.text = "No Athlete to show"
        //center the text
        messageLabel.textAlignment = .Center
        //auto size the text
        messageLabel.sizeToFit()
        //set back to label view
        tableView.backgroundView = messageLabel;
        //no separator
        tableView.separatorStyle = .None;
    }

   
}


