//
//  SettingsViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 18/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import TSMessages



let PASSWORD_FIELD_TAG = 2
let WEIGHT_FIELD_TAG = 5
let HEIGHT_FIELD_TAG = 4

let heightFeetArray = ["1","2","3","4","5","6","7","8","9"]
let heightInchArray = ["0","1","2","3","4","5","6","7","8","9","10","11"]

struct  ProfileDetails {
    
    var propertyName:String
    var isEditable:Bool
    var isSecure:Bool
    var propertyValue:String?
    var isNumeric:Bool
    
    init(propertyName:String,isEditable:Bool,isSecure:Bool,isNumeric:Bool)
    {
        self.propertyName = propertyName
        self.isEditable = isEditable
        self.isSecure = isSecure
        self.isNumeric = isNumeric
    }
    
}



class SettingsViewController : TRKRBaseViewController
{
    @IBOutlet var profileHeaderView: ProfileHeaderView!
    @IBOutlet var athleteTableView: UITableView!
    var lstProfileDetails = [ProfileDetails]()
    var currentViewMode:ViewMode?
    var currentUserProfileDetails:UserProfileDetails?
    let profileManager = ProfileManager()
    var userDict:Dictionary<String,String>!
    var userDetailFromOtherVC : FanDetails?
    let myPickerController = UIImagePickerController()
    var profileImageUrl : String?
    
    //MARK: View Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        TRKRUtility.setBGColor(self.view)
        
        
        self.modalPresentationStyle = .CurrentContext;
        self.modalPresentationStyle = .FormSheet;
        
        
        myPickerController.delegate = self;
        
        
        profileHeaderView = ProfileHeaderView()
        TRKRUtility.setCustomBorder(profileHeaderView)
        self.view.sendSubviewToBack(profileHeaderView)
        setViewMode()
        lstProfileDetails = getProfileDetailsArray()
        checkEditPermission()
        TRKRUtility.removeEmptyCellsInTableView(athleteTableView)
        //Register used nib
        let nib = UINib(nibName: "ProfileDetailCell", bundle: nil)
        athleteTableView.registerNib(nib, forCellReuseIdentifier: PROFILE_DETAIL_CELL)
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        //  self.view.clipsToBounds = true
        if currentViewMode == .AthleteViewMode
        {
            if userDetailFromOtherVC == nil {
                getAthleteProfile()
            }
            else {
                getFanProfile()
            }
            
        }
        else
        {
            getFanProfile()
        }
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        
        userDetailFromOtherVC = nil
    }
    
    
    func checkEditPermission()
    {
        if userDetailFromOtherVC != nil {
            for (index,_) in lstProfileDetails.enumerate()
            {
                lstProfileDetails[index].isEditable = false
            }
            profileHeaderView.editButton.hidden = true
        }
        else{
            profileHeaderView.delegate = self
        }
    }
    
    
    func setViewMode()
    {
        if UserProfileManager.sharedInstance.getSharedUserProfile().user_role! == "1"
        {
            currentViewMode = ViewMode.AthleteViewMode
        }
        else
        {
            currentViewMode = ViewMode.FanViewMode
        }
        
        if userDetailFromOtherVC == nil {
            setNavigationController(title: NavigationBarTitle.Settings)
        }
        else
        {
            setNavigationControllerWithBack(title: NavigationBarTitle.Settings)
        }
    }
    
    
    func setUserDetailsFromOtherViewController(_UserDetails : FanDetails)
    {
        userDetailFromOtherVC = _UserDetails
    }
    
    
    func saveAction() -> Void{
        //Handle Validations
        
        let heightField :UITextField = self.view.viewWithTag(HEIGHT_FIELD_TAG) as! UITextField
        var height = heightField.text!
        
        height = height.stringByReplacingOccurrencesOfString("\"", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        height = height.stringByReplacingOccurrencesOfString("\'", withString: ".", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        let weightField :UITextField = self.view.viewWithTag(WEIGHT_FIELD_TAG) as! UITextField
        let weight = weightField.text!
        
        let pwdField :UITextField = self.view.viewWithTag(PASSWORD_FIELD_TAG) as! UITextField
        let password = pwdField.text!
        
        if currentViewMode == .AthleteViewMode
        {
            if (height.characters.count == 0 || weight.characters.count == 0 || password.characters.count == 0)
            {
                showNotificationMessageWithTypeOfNotifcation("Invalid fields.Please provide valid inputs.", notificationtype: TSMessageNotificationType.Error)
                return
            }
        }
        else {
            if password.characters.count == 0 {
                showNotificationMessageWithTypeOfNotifcation("Please provide password", notificationtype: TSMessageNotificationType.Error)
                return
            }
        }
        
        print("Updated values ", height, weight, password)
        
        updateUserProfile(password,height: height,weight: weight)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getProfileDetailsArray()-> [ProfileDetails]
    {
        var listProfileDetails = [ProfileDetails]()
        
        let name         = ProfileDetails(propertyName: "Name:", isEditable: false, isSecure: false,isNumeric: false)
        let email        = ProfileDetails(propertyName: "Email:", isEditable: false, isSecure: false,isNumeric: false )
        let password     = ProfileDetails(propertyName: "Password:", isEditable: true, isSecure: true,isNumeric: false)
        let dob          = ProfileDetails(propertyName: "Date Of Birth:", isEditable: false, isSecure: false,isNumeric: false)
        let height       = ProfileDetails(propertyName: "Height:", isEditable: true, isSecure: false,isNumeric: true)
        let weight       = ProfileDetails(propertyName: "Weight:", isEditable: true, isSecure: false,isNumeric: true)
        let gender       = ProfileDetails(propertyName: "Gender:", isEditable: false, isSecure: false,isNumeric: false)
        let sport        = ProfileDetails(propertyName: "Sport:", isEditable: false, isSecure: false,isNumeric: false)
        let position     = ProfileDetails(propertyName: "Position:", isEditable: false, isSecure: false,isNumeric: false)
        
        let userType     = ProfileDetails(propertyName: "User Type:", isEditable: false, isSecure: false,isNumeric: false)
        let city         = ProfileDetails(propertyName: "City:", isEditable: false, isSecure: false,isNumeric: false)
        let state        = ProfileDetails(propertyName: "State:", isEditable: false, isSecure: false,isNumeric: false)
        let favoriteTeam = ProfileDetails(propertyName: "Favorite Team:", isEditable: false, isSecure: false,isNumeric: false)
        
        if currentViewMode == ViewMode.AthleteViewMode
        {
            if userDetailFromOtherVC != nil {
                listProfileDetails+=[name,email,dob,height,weight,gender,sport,position]
            }else
            {
                listProfileDetails+=[name,email,password,dob,height,weight,gender,sport,position]
            }
        }
        else
        {
            listProfileDetails+=[name,email,password,userType,gender,city,state,favoriteTeam]
        }
        return listProfileDetails
    }
    
    
    func getAthleteDictionary() -> Dictionary<String,String>
    {
        var dict:Dictionary<String,String> = Dictionary()
        dict["Name:"] = currentUserProfileDetails?.user_name?.capitalizeFirst
        dict["Email:"] = currentUserProfileDetails?.user_email
        dict["Password:"] = currentUserProfileDetails?.user_password
        dict["Date Of Birth:"] = currentUserProfileDetails?.user_dob
        if let height = currentUserProfileDetails?.user_height
        {
            dict["Height:"] = TRKRUtility.getFormattedHeight(height,page:"Settings")
        }
        else
        {
            dict["Height:"] = ""
        }
        dict["Weight:"] = currentUserProfileDetails?.user_weight
        dict["Gender:"] = currentUserProfileDetails?.user_gender
        dict["Sport:"] = currentUserProfileDetails?.user_sport
        dict["Position:"] = currentUserProfileDetails?.position
        return dict
        
    }
    
    func getFanDictionary() -> Dictionary<String,String>
    {
        var dict:Dictionary<String,String> = Dictionary()
        dict["Name:"] = currentUserProfileDetails?.user_name?.capitalizeFirst
        dict["Email:"] = currentUserProfileDetails?.user_email
        dict["Password:"] = currentUserProfileDetails?.user_password
        dict["User Type:"] = currentUserProfileDetails?.user_type
        dict["Gender:"] = currentUserProfileDetails?.user_gender
        dict["City:"] =  TRKRUtility.getFormattedCityName((currentUserProfileDetails?.city!)!)
        if let state_id = currentUserProfileDetails?.state_id
        {
            dict["State:"] = TRKRUtility.getStateShortName(state_id)
        }
        dict["Favorite Team:"] = currentUserProfileDetails?.user_favorite_team
        
        return dict
        
    }
    
    
    
    func getCurrentUserToken() -> UserProfile
    {
        if currentViewMode == .AthleteViewMode
        {
            if userDetailFromOtherVC == nil {
                return UserProfileManager.sharedInstance.getSharedUserProfile()
            }else {
                let userprofile = UserProfile()
                if userDetailFromOtherVC != nil {
                    userprofile.token = UserProfileManager.sharedInstance.getSharedUserProfile().token
                    userprofile.user_id = userDetailFromOtherVC?.user_id
                    return userprofile
                }
            }
        }
        else
        {
            let userprofile = UserProfile()
            if userDetailFromOtherVC != nil {
                userprofile.token = UserProfileManager.sharedInstance.getSharedUserProfile().token
                userprofile.user_id = userDetailFromOtherVC?.user_id
                return userprofile
            }
        }
        
        return UserProfileManager.sharedInstance.getSharedUserProfile()
    }
    
    
    //MARK: API
    func getAthleteProfile() -> Void
    {
        let userProfile = getCurrentUserToken()
        
        if NetworkReachability.isConnectedToNetwork() == true
        {
            profileManager.getAthleteProfile(userProfile, callback: { (response:ProfileDetailResponse) -> () in
                self.parseResponse(response)
                })
                { (error:ErrorResponse) -> () in
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    func parseResponse(response:ProfileDetailResponse) -> Void
    {
        
        if let result = response.result
        {
            currentUserProfileDetails = result.first
            if currentViewMode == .AthleteViewMode
            {
                userDict = getAthleteDictionary()
            }
            else
            {
                userDict = getFanDictionary()
            }
            LogManager.sharedInstance.DLog(userDict)
            
            setProfileImage(currentUserProfileDetails!.user_profile_image, imageView: self.profileHeaderView.profileImageView)
            
            if let profileImage =  currentUserProfileDetails!.user_profile_image {
                profileImageUrl = profileImage
            }
            
            updateUserProfile()
            
            if userDetailFromOtherVC == nil {
                addSaveButton()
            }
            
            self.athleteTableView.reloadData()
        }
    }
    
    
    //MARK: Update Current User SharedInstance. If any changes happen , update local prefernce
    func updateUserProfile()
    {
        if userDetailFromOtherVC == nil {
            let userProfile = getCurrentUserToken()
            userProfile.profile_image = profileImageUrl
            UserProfileManager.sharedInstance.updateUserProfile(userProfile)
        }
    }
    
    //MARK: Set Profile Image
    func setProfileImage(imageUrl : String?,imageView : UIImageView?)
    {
        if imageView != nil {
            TRKRUtility.setProfileImage(imageUrl, imageView: imageView!)
        }
    }
    
    
    func getFanProfile() -> Void
    {
        let userProfile = getCurrentUserToken()
        if NetworkReachability.isConnectedToNetwork() == true
        {
            profileManager.getAthleteProfile(userProfile, callback: { (response:ProfileDetailResponse) -> () in
                self.parseResponse(response)
                })
                { (error:ErrorResponse) -> () in
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
        
    }
    
    //MARK: API
    func updateUserProfile(password:String,height:String,weight:String) -> Void
    {
        let userProfileDetails:UserProfileDetails =  UserProfileDetails()
        let user:UserProfile = UserProfileManager.sharedInstance.getSharedUserProfile()
        userProfileDetails.token = user.token
        userProfileDetails.user_id = user.user_id
        userProfileDetails.user_password = password
        userProfileDetails.user_height = height
        userProfileDetails.user_weight = weight
        
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            profileManager.updateAthleteProfile(userProfileDetails, callback: { (response:SuccessResponse) -> () in
                self.parseUpdateProfileResponse(response)
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                })
                { (error:ErrorResponse) -> () in
                    LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    
    func parseUpdateProfileResponse(response:SuccessResponse)-> Void
    {
        if response.status == API_SUCCESS_STATUS_CODE
        {
            //TRKRUtility.showAlertMessage(kAPP_TITLE, message: "Your Profile has been successfully updated.", viewController: self)
            showNotificationMessageWithTypeOfNotifcation("Your Profile has been successfully updated.", notificationtype: TSMessageNotificationType.Success)
        }
        if currentViewMode == .AthleteViewMode
        {
            getAthleteProfile()
        }
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

//MARK: Table View delegate
extension SettingsViewController:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 150
        }
        return 0
        
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            return profileHeaderView
        }
        return nil
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 0
        }
        guard let _ = currentUserProfileDetails else
        {
            return 0
        }
        return lstProfileDetails.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:ProfileDetailCell = tableView.dequeueReusableCellWithIdentifier(PROFILE_DETAIL_CELL, forIndexPath: indexPath) as! ProfileDetailCell
        let profileDetail = lstProfileDetails[indexPath.row]
        setEditableProperties(cell,profileDetail: profileDetail)
        cell.profiePropertiesLabel.text = profileDetail.propertyName
        cell.profileValueTextField.text = userDict[profileDetail.propertyName]
        cell.profileValueTextField.tag = indexPath.row
        cell.profileValueTextField.delegate = self
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func  setEditableProperties(cell:ProfileDetailCell,profileDetail:ProfileDetails)
    {
        if profileDetail.isEditable
        {
            cell.profileValueTextField.enabled = true
        }
        else
        {
            cell.profileValueTextField.enabled = false
        }
        
        if profileDetail.isSecure
        {
            cell.profileValueTextField.secureTextEntry = true
        }
        else
        {
            cell.profileValueTextField.secureTextEntry = false
        }
        
        if profileDetail.isNumeric
        {
            cell.profileValueTextField.keyboardType = .DecimalPad
        }
        else
        {
            cell.profileValueTextField.keyboardType = UIKeyboardType.Default
        }
    }
}




extension SettingsViewController:UITextFieldDelegate
{
    //Delegate method used to clear
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if textField.tag == PASSWORD_FIELD_TAG
        {
            textField.text = nil
        }
        if textField.tag == HEIGHT_FIELD_TAG
        {
            let str = getFormattedHeight(textField.text!)
            let hFeetIndex =    heightFeetArray.indexOf(str.0)
            let hInchIndex = heightInchArray.indexOf(str.1)
            
            textField .resignFirstResponder()
            ActionSheetMultipleStringPicker.showPickerWithTitle("Select Feet & Inch", rows: [
                heightFeetArray ,
                heightInchArray
                ], initialSelection: [hFeetIndex!, hInchIndex!], doneBlock: {
                    picker, values, indexes in
                    print("\(values)")
                    print("\(indexes)")
                    let FirstValue = "\(values[0])"
                    let SecondValue = "\(values[1])"
                    let intOfFirstValue = Int(FirstValue)! + 1
                    let intOfSeccondValue = Int(SecondValue)!
                    let combine = "\(intOfFirstValue)"+"'"+"\(intOfSeccondValue)"+"\""
                    textField.text = combine
                    return
                }, cancelBlock: { ActionMultipleStringCancelBlock in return }, origin: textField)
        }
    }
    
    //MARK: Text field delegate methods
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag == PASSWORD_FIELD_TAG
        {
            if textField.text?.characters.count == 0
            {
                textField.text = currentUserProfileDetails?.user_password
            }
        }
    }
    
    func showPasswordValidationMessage() -> Void
    {
        //TRKRUtility.showAlertMessage(kAPP_TITLE, message: "Password must contain at least six characters!", viewController: self)
        showNotificationMessageWithTypeOfNotifcation("Password must contain at least six characters!", notificationtype: TSMessageNotificationType.Warning)
    }
    
    
    //MARK: Height Formatter
    func getFormattedHeight(height:String) ->(String,String)
    {
        var str = height.componentsSeparatedByString("'")
        let truncated = str[1].substringToIndex(str[1].endIndex.predecessor())
        str[1] = truncated
        print(str)
        return (str[0],str[1])
        
    }
    
    
    
    
}


extension SettingsViewController: UIImagePickerControllerDelegate,ProfileHeaderViewDelegate,UINavigationControllerDelegate{
    
    //MARK: ProfileHeaderView Delegate
    func editProfileButtonDelegate(sender: UIButton)
    {
        myPickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        myPickerController.allowsEditing = false
        presentViewController(myPickerController, animated: true, completion: nil)
    }
    
    
    //MARK: Image Upload API
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var customImageData : NSData? = NSData()
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            customImageData = UIImageJPEGRepresentation( pickedImage, 0.2 )
        }
        dismissViewControllerAnimated(true, completion: nil)
        
        if customImageData != nil{
            let userProfile =  getCurrentUserToken()
            userProfile.profileImageData = customImageData
            
            if NetworkReachability.isConnectedToNetwork() == true
            {
                LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
                profileManager.uploadImage(userProfile, callback: { (response) -> () in
                    LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.parseImageUploadResponse(response)
                    }, failure: { (error) -> () in
                        print("Error block")
                        LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                })
            }
            else
            {
                showNetworkConnectivityError()
            }
        }
    }
    
    func parseImageUploadResponse(response:SuccessResponse)
    {
        SDImageCache.sharedImageCache().removeImageForKey(profileImageUrl, fromDisk: true)
        self.viewWillAppear(true)
        LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
        
    }
    
    
}










