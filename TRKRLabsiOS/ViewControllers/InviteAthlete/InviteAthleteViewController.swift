//
//  InviteAthleteViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 31/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import Masonry
import TSMessages

let noCharToSearch : Int = 3
let clearTextfield : String = "xxyyyuuiii343"

let titleText = "ENTER ATHLETES EMAIL ADDRESS"
let para1 = "REQUEST TO ACCESS AN ATHLETES"
let para2 = "PROFILE TO VIEW THEIR PERFORMANCE"
let para3 = "AND STATISTICS"


class InviteAthleteViewController: TRKRBaseViewController{

    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var para1Label : UILabel!
    @IBOutlet weak var para2Label : UILabel!
    @IBOutlet weak var para3Label : UILabel!
    @IBOutlet weak var emailTextfield : UITextField!
    @IBOutlet weak var requestToFollowButton : UIButton!
    
    var athleteManager = InviteAthleteManager()
    var athleteArray:[FanDetails]?  = [FanDetails]()

    @IBOutlet weak var searchTexfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        TRKRUtility.setBGColor(self.view)
        setNavigationControllerWithBack(title: NavigationBarTitle.InviteAthletes)
        
        configTitleLable()
        configUILable(para1Label, textInput:para1 )
        configUILable(para2Label, textInput:para2 )
        configUILable(para3Label, textInput:para3 )
        
        
        requestToFollowButton.layer.cornerRadius = 5.0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func getCurrentUserProfile() -> UserProfile?{
        
        let currentUserProfile : UserProfile?
        guard let user =   UserProfileManager.sharedInstance.getUserProfile() else
        {
            currentUserProfile = UserProfileManager.sharedInstance.getSharedUserProfile()
            return currentUserProfile
        }
        currentUserProfile = user
        
        return currentUserProfile
    }
    
    //MARK: Get Athletes API
    func getAthleteListFromSearchText(searchText:String) -> Void
    {
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            athleteManager.getAthleteList(searchText, userProfile: getCurrentUserProfile()!, callback: { (response) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.parseResponse(response)
                }) { (error:ErrorResponse) -> () in
                    LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.emailTextfield.text = ""
                    self.showNotificationMessageWithTypeOfNotifcation("Couldn't processing your request, please try again later ", notificationtype: TSMessageNotificationType.Error)
                    self.failureBlockHandler(error)
                }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    func parseResponse(response:FansListResponse)
    {
        emailTextfield.text = ""
        
        if let dequeResponse = response.message {
            if dequeResponse == "Already Requested" {
                showNotificationMessageWithTypeOfNotifcation("You already requested the Athlete ", notificationtype: TSMessageNotificationType.Success)
            }
            else {
                showNotificationMessageWithTypeOfNotifcation("Your request sent to the Athlete", notificationtype: TSMessageNotificationType.Success)
            }
        }
        
    }
    
    
    
    func configTitleLable(){
        // Setting Text for label and underline
        let attributedString : NSMutableAttributedString  = NSMutableAttributedString(string: titleText)
        let rangeOfString : NSRange = NSMakeRange(0, attributedString.length)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(int: 1), range: rangeOfString )
        
        //Setting letter spacing
        let spacing = 1.5
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: rangeOfString)
        titleLabel.attributedText = attributedString
        titleLabel.textAlignment = .Center
        titleLabel.textColor = fontColor
    }
    
    
  
    func configUILable(labelRef:UILabel,textInput:String){
        
        let attributedString : NSMutableAttributedString  = NSMutableAttributedString(string:textInput )
        let rangeOfString : NSRange = NSMakeRange(0, attributedString.length)
        let spacing = 1.5
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: rangeOfString)
     
        let fontForLabel = UIFont(name: fontFamiliy , size: 13)
        let attrsDictionary = NSDictionary(object: fontForLabel!, forKey: NSFontAttributeName)
        attributedString.addAttributes(attrsDictionary as! [String : AnyObject], range: rangeOfString)
        
        labelRef.attributedText = attributedString
        labelRef.textAlignment = .Center
        labelRef.textColor = fontColor
        
        
    }
    
    func getPreferredFontStyleForDevice()->String
    {
        let deviceType = UIDevice.currentDevice().deviceType
        
        switch deviceType {
        case .IPhone6SPlus: return UIFontTextStyleSubheadline
        case .IPhone4 : return UIFontTextStyleFootnote
        default:return UIFontTextStyleSubheadline
        }
        
    }
    
    
    @IBAction func RequestToFollowAction(sender: AnyObject) {
        
        let email = emailTextfield.text!
        if TRKRUtility.isValidEmail(email){
            getAthleteListFromSearchText(email)
        }else
        {
            showNotificationMessageWithTypeOfNotifcation("Please enter valid email address", notificationtype: TSMessageNotificationType.Error)
        }
    }
    
    
}


extension InviteAthleteViewController : UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
        if string == " " {
            return true
        }
        
        let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        if newString.characters.count > 0 {

        }
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {

        return true
    }
}