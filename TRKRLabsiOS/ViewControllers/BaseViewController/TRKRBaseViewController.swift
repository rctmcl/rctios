//
//  TRKRBaseViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 10/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import TSMessages

enum NavigationBarTitle:String
{
    case Logo        = "TRKR LABS"
    case Settings    = "Settings"
    case MyAthletes  = "My Athletes"
    case Stats       = "Stats"
    case Fans        = "Fans"
    case Fundraising = "Fundraising"
    case InviteAthletes = "Invite Athletes"
    case ForgotPassword = "Forgot Password?"
    case NewPassword    = "New Password"
}

class TRKRBaseViewController: UIViewController,TSMessageViewProtocol
{
    @IBOutlet var bgImageView : UIImageView!
    
    override func viewDidLoad() -> Void
    {
       // self.view.clipsToBounds = true
        TSMessage.setDefaultViewController(self)
        TSMessage.setDelegate(self)
    }
    
    override func viewWillAppear(animated: Bool) -> Void
    {
       
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        if bgImageView != nil {
            bgImageView.hidden = true
        }
    }
    
    
    
    //MARK: This method used to hide keyboard whenever user touch empty screen
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: Toggle Slide Navigation Menu Action
    func toggleSlideMenu() -> Void
    {
        let appDelegate : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.mmDrawerController!.toggleDrawerSide(MMDrawerSide.Left, animated: true, completion: nil)
    }
    
    
    //MARK: Back Button Touch
    func backButtonTouch() -> Void
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setNavigationController(title title:NavigationBarTitle) -> Void
    {
        let lBtn = UIButton(type: .Custom)
        lBtn.addTarget(self, action: Selector("toggleSlideMenu"), forControlEvents: UIControlEvents.TouchUpInside)
        lBtn.frame = CGRectMake(0, 0, 30, 30)
        let image = UIImage(named: "MenuIconImage")
        lBtn .setImage(image, forState: UIControlState.Normal)
        let barButton = UIBarButtonItem(customView: lBtn)
        self.navigationItem.leftBarButtonItem = barButton
        self.navigationController!.navigationBar.barTintColor = navigationBarColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(),NSFontAttributeName : UIFont(name: "HelveticaNeue-Light", size: 18)!]
        self.navigationController?.navigationBar.translucent = false
        setNavigationBorderColor()
        
        switch (title)
        {
            case .Fans:
                self.navigationItem.title = title.rawValue
            case .Fundraising:
                self.navigationItem.title = title.rawValue
            case .Settings:
                self.navigationItem.title = title.rawValue
            case .Stats:
                self.navigationItem.title = title.rawValue
            case .InviteAthletes:
                self.navigationItem.title = title.rawValue
            case .MyAthletes:
                self.navigationItem.title = title.rawValue
            case .ForgotPassword:
                self.navigationItem.title = title.rawValue
            case .NewPassword:
                self.navigationItem.title = title.rawValue
            default:
                self.navigationItem.titleView = UIImageView(image: UIImage(named:"TRKRNavLogo"))
        }
    }
    
    func setNavigationBorderColor() -> Void
    {
        if let navigationController = self.navigationController {
            let navigationBar = navigationController.navigationBar
            let navBorder: UIView = UIView(frame: CGRectMake(0, navigationBar.frame.size.height - 1, navigationBar.frame.size.width, 1))
            // Set the color you want here
            navBorder.backgroundColor = UIColor.whiteColor()
            navBorder.opaque = true
            self.navigationController?.navigationBar.addSubview(navBorder)
        }
    }
    
    func setNavigationControllerWithBack(title title:NavigationBarTitle)->Void
    {
        let lBtn = UIButton(type: .Custom)
        lBtn.addTarget(self, action: Selector("backButtonTouch"), forControlEvents: UIControlEvents.TouchUpInside)
        lBtn.frame = CGRectMake(0, 0, 30, 30)
        let image = UIImage(named: "BackButtonImage")
        lBtn .setImage(image, forState: UIControlState.Normal)
        let barButton = UIBarButtonItem(customView: lBtn)
        self.navigationItem.leftBarButtonItem = barButton
        self.navigationController!.navigationBar.barTintColor = navigationBarColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(),NSFontAttributeName : UIFont(name: "HelveticaNeue-Light", size: 18)!]
        self.navigationController?.navigationBar.translucent = false
        setNavigationBorderColor()
        
        switch (title)
        {
        case .Fans:
            self.navigationItem.title = title.rawValue
        case .Fundraising:
            self.navigationItem.title = title.rawValue
        case .Settings:
            self.navigationItem.title = title.rawValue
        case .Stats:
            self.navigationItem.title = title.rawValue
        case .InviteAthletes:
            self.navigationItem.title = title.rawValue
        case .MyAthletes:
            self.navigationItem.title = title.rawValue
        case .ForgotPassword:
            self.navigationItem.title = title.rawValue
        case .NewPassword:
            self.navigationItem.title = title.rawValue
        default:
            self.navigationItem.titleView = UIImageView(image: UIImage(named:"TRKRNavLogo"))
        }
    }
    
    //MARK : API FAILURE HANDLER
    func failureBlockHandler(error:ErrorResponse) -> Void
    {
        let errorMessage:String!

        if error.errorCode! == 1004 || error.errorCode! == -6004 || error.errorCode < 0 {
            //TRKRUtility.showAlertMessage(kAPP_TITLE, message: "Could not connect to server ", viewController: self)
            errorMessage = " Could not connect to server.Please try again later"
        }
        else if error.errorCode! == 400
        {
            errorMessage = "Internal Server Error"
        }
        else if error.errorCode! == 401
        {
            errorMessage = "Internal Server Error"
        }
        else if error.errorCode! == 404
        {
            errorMessage = "Internal Server Error"
        }
        else if error.errorCode! == 500
        {
            errorMessage = "Internal Server Error"
        }
        else if error.errorCode! == 503
        {
            errorMessage = "Internal Server Error"
        }
        else {
            //TRKRUtility.showAlertMessage(kAPP_TITLE ,message: error.errorDescription!,viewController: self)
            errorMessage = error.errorDescription!
        }
        showNotificationMessageWithTypeOfNotifcation(errorMessage, notificationtype: TSMessageNotificationType.Error)
    }
    
    func addSaveButton() -> Void{
        let rButton = UIButton(frame: CGRectMake(0,0,60,30))
        rButton.setTitle("Save", forState: .Normal)
        rButton.titleLabel?.font = UIFont(name: TRKR_DEFAULT_FONT, size: 14)
        rButton.addTarget(self, action: "saveAction", forControlEvents: .TouchUpInside)
         self.navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: rButton)
    }

    
    func setNavigationRightButton(){
        
        let lBtn = UIButton(type: .Custom)
        lBtn.addTarget(self, action: Selector("inviteAthleteButtonTouch:"), forControlEvents: UIControlEvents.TouchUpInside)
        lBtn.frame = CGRectMake(0, 0, 30, 30)
        let image = UIImage(named: "PlusIcon")
        lBtn .setImage(image, forState: UIControlState.Normal)
        
        let barButton = UIBarButtonItem(customView: lBtn)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController!.navigationBar.barTintColor = navigationBarColor
        self.navigationController?.navigationBar.translucent = false
        
    }
    
    func inviteAthleteButtonTouch(sender : AnyObject)
    {
        let inviteAthletVC : InviteAthleteViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_INVITEATHLETE_VC) as! InviteAthleteViewController
        self.navigationController?.pushViewController(inviteAthletVC, animated: true)
    }
  
    func showNotificationMessageWithTypeOfNotifcation(message:String,notificationtype:TSMessageNotificationType)
    {
        NotificationDismiss()
        TSMessage.showNotificationInViewController(self.navigationController, title: kAPP_TITLE, subtitle: message, image: nil, type: notificationtype, duration: 0, callback: nil, buttonTitle: nil, buttonCallback: nil, atPosition: TSMessageNotificationPosition.NavBarOverlay, canBeDismissedByUser: true)
        
    }
    
    
    func NotificationDismiss()
    {
        if TSMessage.isNotificationActive()
        {
            TSMessage.dismissActiveNotification()
        }
    }
    func showNetworkConnectivityError()
    {
        NotificationDismiss()
        TSMessage.showNotificationInViewController(self.navigationController, title: "Network Error", subtitle: "Couldn't connect the server.Check your network connection", image: nil, type: TSMessageNotificationType.Error, duration: 0, callback: nil, buttonTitle: nil, buttonCallback: nil, atPosition: TSMessageNotificationPosition.NavBarOverlay, canBeDismissedByUser: true)
    }
    
    
    
    
    deinit
    {
        TSMessage.setDelegate(nil)
    }
    
    
    //MARK: States List API
    func getStatesList() -> Void
    {
        
        if ((UserProfileManager.sharedInstance.statesList != nil) && (UserProfileManager.sharedInstance.statesList?.count > 0)) {
            return;
        }
        
        if NetworkReachability.isConnectedToNetwork() == true
        {
            let fanManager = FansManager()
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            fanManager.getStatesList({ (response:StatesListResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                self.parseStatesResponse(response)
                })
                {(error:ErrorResponse) -> () in
                    LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.failureBlockHandler(error)
            }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    func parseStatesResponse(response:StatesListResponse)
    {
        if let states:[StateDetail] = response.result!
        {
            UserProfileManager.sharedInstance.statesList = states
            let appDelegate :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let menuVC:MenuViewController = (appDelegate.mmDrawerController?.leftDrawerViewController as? MenuViewController)!
            menuVC.menuTableView.reloadData()
            
        }
    }
}
