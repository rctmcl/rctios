//
//  FanOrAthleteModeViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 10/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import ChameleonFramework

class FanOrAthleteModeViewController: TRKRBaseViewController {
    
   
    override func viewDidLoad() {
        
        TRKRUtility.setBGColor(self.view)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        if let navigationController = self.navigationController
        {
            navigationController.navigationBarHidden = true
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @IBAction func athleteLoginAction(sender: UIButton) {
         loginAction()
    }
    
    
    @IBAction func fanLoginAction(sender: AnyObject) {
        loginAction()
    }
    
    
    
    func loginAction() -> Void
    {
        let loginVC :LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_LOGIN_VC) as! LoginViewController
        if let navigationController = self.navigationController
        {
            navigationController.pushViewController(loginVC, animated: true)
        }
    }
    
    
    

}
