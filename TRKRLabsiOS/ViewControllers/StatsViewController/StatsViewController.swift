//
//  StatsViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 12/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import Masonry



class StatsViewController: TRKRBaseViewController,ChartViewGetUserProfileDelegate {
    
    @IBOutlet var scrollView    : UIScrollView!
    @IBOutlet var contentView   :UIView!
    @IBOutlet var chartView     :ChartView!
    var selectedUserProfile     : FanDetails?
    let athleteStatsManager     = AthleteStatsManager()
    var athleteStatsData        : [AthleteStatsData]?


    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationController(title: NavigationBarTitle.Stats)
        TRKRUtility.setBGColor(self.view)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        chartView.userProfileDelegate = self;
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func setSelectedUserProfile(_userProfile : FanDetails?)
    {
        selectedUserProfile = _userProfile
    }

    
    func getCurrentUserProfile() ->UserProfile?
    {
        var currentUserProfile:UserProfile? = UserProfileManager.sharedInstance.getUserProfile()
        if currentUserProfile == nil
        {
            let userProfile:UserProfile = UserProfile()
            let token :String = NSUserDefaults.standardUserDefaults().valueForKey(kUserToken) as! String
            let userId:String = NSUserDefaults.standardUserDefaults().valueForKey(kUserId) as! String
            userProfile.user_id = userId
            userProfile.token = token
            currentUserProfile = userProfile
        }
        
        if currentUserProfile?.user_role != "1"
        {
            if selectedUserProfile != nil{
                let userDetails  = UserProfile()
                userDetails.user_id = selectedUserProfile?.user_id
                userDetails.token = currentUserProfile?.token
                return userDetails
            }
        }
        return currentUserProfile
    }
    
    
    //MARK : ChartView Delegate Methods
    func getUserProfile()->UserProfile?
    {
        return getCurrentUserProfile()
    }
    
    func chartViewResponseErrorBlock(error:ErrorResponse?)->Void
    {
        self.failureBlockHandler(error!)
    }
    
   

}
