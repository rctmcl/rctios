//
//  FanDashboardViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 28/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

class FanDashboardViewController: DashboardViewController,ChartViewGetUserProfileDelegate {

    
    @IBOutlet weak var athleteChartView: ChartView!
    @IBOutlet weak var athleteDashboard: DashboardView!
    var userDetails: FanDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationControllerWithBack(title: NavigationBarTitle.Logo)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        dashboardScrollView.scrollEnabled = true
        athleteChartView.userProfileDelegate = self;
    }

    override func viewDidAppear(animated: Bool)
    {
       // super.viewDidAppear(true)
        getDashboardData(getCurrentUserProfile())
    }
    
    func getCurrentUserProfile()->UserProfile?
    {
        currentUserProfile = UserProfileManager.sharedInstance.getUserProfile()
        if currentUserProfile == nil
        {
            currentUserProfile  = UserProfileManager.sharedInstance.getSharedUserProfile()
            LogManager.sharedInstance.DLog(currentUserProfile?.token)
        }
        
        if currentUserProfile?.user_role != "1"
        {
            if userDetails != nil{
                let selectedUser = UserProfile()
                selectedUser.user_id = userDetails?.user_id
                selectedUser.token = currentUserProfile?.token
               return selectedUser
            }
        }else {
            return currentUserProfile
        }
        
         return currentUserProfile
    }
    
    
    func setUserDetails(selectedUserProfile:FanDetails?)
    {
        userDetails = selectedUserProfile
    }
    
    //MARK : ChartView Delegate Methods
    func getUserProfile()->UserProfile?
    {
        return getCurrentUserProfile()
    }
    
    func chartViewResponseErrorBlock(error:ErrorResponse?)->Void
    {
        self.failureBlockHandler(error!)
    }
    
}
