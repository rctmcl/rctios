//
//  DashboardViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 14/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import ChameleonFramework
import TSMessages

class DashboardViewController: TRKRBaseViewController
{
    var fanManager = FansManager()
    let dbManager = DashboardManager()
    var currentUserProfile:UserProfile?
    var dashboardData:DashboardData?
    
    @IBOutlet var dashboardScrollView: UIScrollView!
    @IBOutlet var athleteDashboardView: DashboardView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
                // Do any additional setup after loading the view.
        setNavigationController(title: NavigationBarTitle.Logo)
        TRKRUtility.setBGColor(self.view)
        let size:CGSize = PlaceholderManager.sharedInstance.getCGSizeOfView(view: athleteDashboardView.athleteProfileImageView)
        athleteDashboardView.athleteProfileImageView.image = PlaceholderManager.sharedInstance.getPlaceHolderImageForSize(size:size)
       
       
    }

    override func viewWillAppear(animated: Bool) {
        if UIScreen.mainScreen().bounds.height > 485
        {
            dashboardScrollView.scrollEnabled = false
        }
      
        self.view.alpha = 1.0
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        self.view.alpha = 0.0
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        currentUserProfile = UserProfileManager.sharedInstance.getUserProfile()
        if currentUserProfile == nil
        {
          currentUserProfile  = UserProfileManager.sharedInstance.getSharedUserProfile()
          LogManager.sharedInstance.DLog(currentUserProfile?.token)
        }
        getDashboardData(currentUserProfile)
        
        setProfileImage(currentUserProfile?.profile_image, imageView:  athleteDashboardView.athleteProfileImageView)
    }
    
    //MARK:  SetProfileImage
    func setProfileImage(imageUrl : String?,imageView : UIImageView?)
    {
        if imageView != nil {
            TRKRUtility.setProfileImage(imageUrl, imageView: imageView!)
        }
    }
    
    //MARK : API
    func getDashboardData(userprofile:UserProfile?) -> Void
    {
        guard let currentUser = userprofile else
        {
            return
        }
        currentUser.date = NSDate().description
        if NetworkReachability.isConnectedToNetwork() == true
        {
            LoadingIndicatorManager.sharedInstance.showLoadingIndicator()
            dbManager.getDashboardData(currentUser, callback: { (response:AthleteDashboardResponse) -> () in
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                 self.parseResponse(response)
                })
                { (error:ErrorResponse) -> () in
                    LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                    self.failureBlockHandler(error)
                }
        }
        else
        {
            showNetworkConnectivityError()
        }
    }
    
    func parseResponse(response:AthleteDashboardResponse)
    {
        if let result = response.result
        {
            dashboardData = result.first
            setDashboardValues(dashboardData!)
        }
        else
        {
            showNotificationMessageWithTypeOfNotifcation("No Data found", notificationtype: TSMessageNotificationType.Warning)
        }
        getStatesList()
    }
    
    
    func setDashboardValues(dashboardData:DashboardData)
    {
        athleteDashboardView.setDashboardDataValues(dashboardData)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    
    
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
