//
//  MenuViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 14/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

struct MenuItem {
    var title:String?
    var image:UIImage?
    
    init(title:String,image:UIImage)
    {
        self.title = title
        self.image = image
    }
}

enum ViewMode
{
    case FanViewMode
    case AthleteViewMode
}

class MenuViewController: TRKRBaseViewController {
    
    
    @IBOutlet var menuTableView: UITableView!
    
    var currentViewMode:ViewMode?
    var athleteMenus:[MenuItem]!
    var fanMenus:[MenuItem]!
    var listMenus:[MenuItem]!
    let appDelegate :AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let loginManager = LoginManager()
    var currentUserProfile:UserProfile?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViewMode()
        
        //Nib For Tableview registeration
        let nib = UINib(nibName: "FansListCell", bundle: nil)
        menuTableView.registerNib(nib, forCellReuseIdentifier: FANS_LIST_CELL_ID)
        let nib1 = UINib(nibName: "MenuListCell", bundle: nil)
        menuTableView.registerNib(nib1, forCellReuseIdentifier: MENU_LIST_CELL_ID)
        
        // Set View Mode
        athleteMenus = getAthleteMenuItems()
        fanMenus = getFanMenuItems()
        setCurrentViewMenu()
        
        //Table view setup
        menuTableView.rowHeight = 60
        menuTableView.estimatedRowHeight = UITableViewAutomaticDimension
        menuTableView.allowsSelection = true
        TRKRUtility.removeEmptyCellsInTableView(menuTableView)
        
        
        //Color setup
        menuTableView.backgroundColor = UIColor.clearColor()
        TRKRUtility.setBGColor(self.view)
        
        
        
    }
    
    
   override func viewWillAppear(animated: Bool) {
        currentUserProfile = UserProfileManager.sharedInstance.getUserProfile()
        menuTableView.reloadData()
    }
    
    func setViewMode()
    {
        if UserProfileManager.sharedInstance.getSharedUserProfile().user_role! == "1"
        {
            currentViewMode = ViewMode.AthleteViewMode
        }
        else
        {
            currentViewMode = ViewMode.FanViewMode
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        // menuTableView.tableFooterView = getLogoutView()
    }
    
    func setCurrentViewMenu() -> Void
    {
        if currentViewMode == .AthleteViewMode
        {
            listMenus = athleteMenus
        }
        else
        {
            listMenus = fanMenus
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getAthleteMenuItems() -> [MenuItem]
    {
        var menuItems = [MenuItem]()
        
        let menuItem1 = MenuItem(title: "DASHBOARD",image: UIImage(named: "DashboardIconImage")!)
        let menuItem2 = MenuItem(title: "STATS",image: UIImage(named: "StatsIconImage")!)
        let menuItem3 = MenuItem(title: "FANS",image: UIImage(named: "FansIconImage")!)
        let menuItem4 = MenuItem(title: "SETTINGS",image: UIImage(named: "SettingsIconImage")!)
        
        menuItems += [menuItem1,menuItem2,menuItem3,menuItem4]
        return menuItems
        
    }
    
    func getFanMenuItems() -> [MenuItem]
    {
        var menuItems = [MenuItem]()
        let menuItem1 = MenuItem(title: "MY ATHLETES",image: UIImage(named: "FansIconImage")!)
        let menuItem2 = MenuItem(title: "FUNDRAISING ",image: UIImage(named: "DonationIconImage")!)
        let menuItem3 = MenuItem(title: "SETTINGS",image: UIImage(named: "SettingsIconImage")!)
        menuItems += [menuItem1,menuItem2,menuItem3]
        return menuItems
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: TableView Methods

extension MenuViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return listMenus.count
        }
        else
        {
            return 0
        }
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            return getFanListCellForIndexPath(indexPath,tableView: tableView)
        }
        else
        {
            let menuItem:MenuItem = listMenus[indexPath.row]
            return getMenuCellForIndexPath(indexPath, tableView: tableView,menuItem: menuItem)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if indexPath.section == 0
        {
            //go to settings page
             var centerNavController:UINavigationController!
            let settingsVC:SettingsViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_SETTINGS_VC) as!
            SettingsViewController
            centerNavController = UINavigationController(rootViewController: settingsVC)
            if centerNavController != nil
            {
                setCenterNavigationController(centerNavController)
            }
        }
        else if indexPath.section == 1
        {
            didSelectRowAtIndexPath(indexPath)
        }
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2
        {
           return 20
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2
        {
            return UIView.init(frame: CGRectZero) // This will create an empty space between logout button and cells
        }
        return nil
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2
        {
            return 50  // Logout button height
        }
        return 0
    }

    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if section == 2
        {
            return getLogoutView()
        }
        return nil
    }
    
      func getLogoutView() -> UIView
      {
        LogManager.sharedInstance.DLog(menuTableView.frame)
        let footerView:UIView = UIView(frame: CGRectMake(0,menuTableView.frame.height-50,menuTableView.frame.width,50))
        let logoutButton:UIButton = UIButton(frame: CGRectMake(0,0,menuTableView.frame.width,31))
        logoutButton.setImage(UIImage(named: "LogoutButtonImage"), forState: .Normal)
        logoutButton.addTarget(self, action: "logoutAction", forControlEvents: .TouchUpInside)
        footerView.addSubview(logoutButton)
        return footerView
    }
    
    //MARK : Did Select Row Page
    
    func didSelectRowAtIndexPath(indexPath:NSIndexPath)
    {
        var centerNavController:UINavigationController!
        if currentViewMode == .AthleteViewMode
        {
            // has 4 rows
            if indexPath.row == 0
            {
                //Dashboard Page
                let dashboardViewController : DashboardViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_DASHBOARD_VC) as! DashboardViewController
               centerNavController = UINavigationController(rootViewController: dashboardViewController)
                
            }
            else if indexPath.row == 1
            {
                //Stats Page
                let statsViewController :StatsViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_STATS_VC) as! StatsViewController
                centerNavController = UINavigationController(rootViewController:statsViewController)
            }
            else if indexPath.row == 2
            {
                //Fans Page
                let fansViewController:FansListViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_FANS_LIST_VC) as! FansListViewController
                centerNavController = UINavigationController(rootViewController: fansViewController)
                
            }
            else if indexPath.row == 3
            {
                //Settings Page
                let settingsVC:SettingsViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_SETTINGS_VC) as!
                SettingsViewController
                centerNavController = UINavigationController(rootViewController: settingsVC)
            }
        } // Fan View Mode
        else
        {
             // has 3 rows
            if indexPath.row == 0
            {
                //My Athlets Page
                let fansViewController:FansListViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_FANS_LIST_VC) as! FansListViewController
                centerNavController = UINavigationController(rootViewController: fansViewController)
            }
            else if indexPath.row == 1
            {
                //Fund Raising Page
                let fansViewController:Fundraising = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_FUNDRAISING_VC) as! Fundraising
                centerNavController = UINavigationController(rootViewController: fansViewController)
                
            }
            else if indexPath.row == 2
            {
                //Settings Page
                //Settings Page
                let settingsVC:SettingsViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_SETTINGS_VC) as!
                SettingsViewController
                centerNavController = UINavigationController(rootViewController: settingsVC)
            }
        }
        if centerNavController != nil
        {
            setCenterNavigationController(centerNavController)
        }
        else
        {
            //close menu
            LogManager.sharedInstance.DLog("View controller not yet set for this view ")
            appDelegate.mmDrawerController!.toggleDrawerSide(.Left, animated: true, completion: nil)
        }
    }
    
    
    func setCenterNavigationController(navigationController:UINavigationController) {
        let centerNavController = navigationController
        appDelegate.mmDrawerController!.centerViewController = centerNavController
        appDelegate.mmDrawerController!.toggleDrawerSide(.Left, animated: true, completion: nil)
    }
    
    
    //MARK: Cells
    
    
    func getMenuCellForIndexPath(indexPath:NSIndexPath,tableView:UITableView,menuItem:MenuItem) -> MenuListCell
    {
        let menuCell:MenuListCell = tableView.dequeueReusableCellWithIdentifier(MENU_LIST_CELL_ID, forIndexPath: indexPath) as! MenuListCell
        menuCell.menuTitleLabel.text = menuItem.title?.capitalizedString
        menuCell.menuIconImageView.image = menuItem.image
        menuCell.selectionStyle = UITableViewCellSelectionStyle.None
        return menuCell
    }
    
    func getFanListCellForIndexPath(indexPath:NSIndexPath,tableView:UITableView) -> UITableViewCell
    {
        let cell:FansListCell = tableView.dequeueReusableCellWithIdentifier(FANS_LIST_CELL_ID) as! FansListCell
        cell.fanNameLabel.text = currentUserProfile?.user_name ?? ""
        if let city = currentUserProfile?.city , stateID =   currentUserProfile?.state
        {
             cell.fanLocationIcon.hidden = false
            let  state = TRKRUtility.getStateShortName(stateID)
            if state != ""
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(city) + ", " + state
            }
            else
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName(city)
            }
        }
        else
        {
            if currentUserProfile?.city != nil
            {
                cell.fanLocationLabel.text = TRKRUtility.getFormattedCityName ((currentUserProfile?.city!)!)
            }
            else
            {
                cell.fanLocationIcon.hidden = true
            }
        }
        setProfileImage(currentUserProfile?.profile_image, imageView: cell.profileImageView)
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    
    //MARK:  SetProfileImage
    func setProfileImage(imageUrl : String?,imageView : UIImageView?)
    {
        if imageView != nil {
            TRKRUtility.setProfileImage(imageUrl, imageView: imageView!)
        }
    }

    
    //MARK: Logout
    func logoutAction() -> Void
    {
        LogManager.sharedInstance.DLog("Logout")
        showLogoutConfirmAlert()
    }
    func showLogoutConfirmAlert() -> Void
    {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to logout?", preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let destroyAction = UIAlertAction(title: "Logout", style: .Destructive) { (action) in
            self.logout()
        }
        alertController.addAction(destroyAction)
        
        self.presentViewController(alertController, animated: true) {
            
        }

    }
    
    //MARK : Logout API
    func logout() -> Void{
        
       // let token = NSUserDefaults.standardUserDefaults().valueForKey(kUserToken) as! String
       /* loginManager.logout(token, callback: { (response:SuccessResponse) -> () in
             self.parseResponse(response)
            })
            { (error:ErrorResponse) -> () in
                self.failureBlockHandler(error)
            }*/
        SDImageCache.sharedImageCache().cleanDisk()
        SDImageCache.sharedImageCache().clearMemory()
        
        UserProfileManager.sharedInstance.clearUserProfile()
        gotoLoginPage()
    }
    
    func parseResponse(response:SuccessResponse)
    {
        UserProfileManager.sharedInstance.clearUserProfile()
        gotoLoginPage()
    }
    
    
    func gotoLoginPage() -> Void
    {
        let fanAthleteVC :FanOrAthleteModeViewController = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_FAN_ATHLETE_VC) as! FanOrAthleteModeViewController
        let navigationController = UINavigationController(rootViewController: fanAthleteVC)
        navigationController.navigationBarHidden = true
        appDelegate.window?.rootViewController = nil
        appDelegate.window?.rootViewController = navigationController
    }
    
}
