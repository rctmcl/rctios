//
//  ForgotPasswordViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 07/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

import UIKit
import TSMessages

class ForgotPasswordViewController: TRKRBaseViewController {
    
    @IBOutlet var forgotPasswordView: ForgotPasswordView!
    @IBOutlet var tempPasswordView: TemporaryPasswordView!
    var forgotpasswordAPIManager = ForgotPasswordManager()

    
    let EmailSubmitMessage = "We will send the Reset Token to your registered Mobile number."
    let EmailNotValidMessage = "Please enter valid email id."
    let EmailNotEnteredMessage = "Please enter your registered email id."
    let PasswordNotEnteredMessage = "Please enter your Reset Token."
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        TRKRUtility.setBGColor(self.view)
        if let navigationController = self.navigationController
        {
            navigationController.navigationBarHidden = false
        }
        setNavigationControllerWithBack(title: NavigationBarTitle.ForgotPassword)

        // Do any additional setup after loading the view.
        
        
        forgotPasswordView.submitButton.addTarget(self, action: "emailSubmitAction", forControlEvents: .TouchUpInside)
        tempPasswordView.passwordSubmitButton.addTarget(self, action: "passwordSubmitAction", forControlEvents: .TouchUpInside)
        //tempPasswordView.passwordSubmitButton.enabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
       
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    //MARK: Button Action
    func emailSubmitAction() -> Void
    {
        self.view.endEditing(true)
        forgotPasswordView.submitButton.enabled = false
        let email = forgotPasswordView.emailTextField.text
        if email?.characters.count > 0
        {
            if TRKRUtility.isValidEmail(email!)
            {
                // API to update Reset Email.
                tempPasswordView.passwordSubmitButton.enabled = true
                sendResetTokenToRegisteredEmail(email!)
            }
            else
            {
                //Email Format Not Valid
                forgotPasswordView.submitButton.enabled = true
                showNotificationMessageWithTypeOfNotifcation(EmailNotValidMessage, notificationtype:.Error)
            }
        }
        else
        {
            //show Enter email Message.
             forgotPasswordView.submitButton.enabled = true
            showNotificationMessageWithTypeOfNotifcation(EmailNotEnteredMessage, notificationtype:.Error)
        }

    }
    
    
    func passwordSubmitAction() -> Void
    {
        self.view.endEditing(true)
       // tempPasswordView.passwordSubmitButton.enabled = false
        
        let pwd = tempPasswordView.tempPasswordTextField.text
        if pwd?.characters.count > 0
        {
        
            checkResetToken(pwd!)
        }
        else
        {
            //show Enter Temporary Password  Message.
            tempPasswordView.passwordSubmitButton.enabled = true
            showNotificationMessageWithTypeOfNotifcation(PasswordNotEnteredMessage, notificationtype:.Error)
        }
    }
    
    
    
    func gotoPasswordResetPage(response:UserProfileDetails) -> Void
    {
        let newPVC = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_NEW_PASSWORD_VC) as? NewPasswordViewController
        if let navigationController = self.navigationController , newPasswordVC = newPVC
        {
            newPasswordVC.setUserDetail(response)
            navigationController.pushViewController(newPasswordVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ForgotPasswordViewController {
    
    //MARK: Reset Token to Registered Email
    func sendResetTokenToRegisteredEmail(emailAddr:String)
    {
        LoadingIndicatorManager.sharedInstance.showindicator()
        forgotpasswordAPIManager.sendResetTokenToRegisteredEmail(emailAddr, callback: { (response) -> () in
            self.parseResponse(response)
            }) { (error) -> () in
                
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                self.forgotPasswordView.emailTextField.text = ""
                if let msg = error.errorDescription {
                self.showNotificationMessageWithTypeOfNotifcation(msg, notificationtype: TSMessageNotificationType.Error)
                }
                else {
                    self.showNotificationMessageWithTypeOfNotifcation("Sorry, having error to send OTP.please try again later", notificationtype: TSMessageNotificationType.Error)
                }
        }
    }
    
    
    //MARK: Check Reset Token
    func checkResetToken(resettoken:String)
    {
        LoadingIndicatorManager.sharedInstance.showindicator()
        forgotpasswordAPIManager.checkResetToken(resettoken, callback: { (response) -> () in
            self.parseCheckResetTokenResponse(response)
            }) { (error) -> () in
                
                LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
                self.tempPasswordView.tempPasswordTextField.text = ""
                if let msg = error.errorDescription {
                    self.showNotificationMessageWithTypeOfNotifcation(msg, notificationtype: TSMessageNotificationType.Error)
                }
                else {
                    self.showNotificationMessageWithTypeOfNotifcation("Sorry, having error to check your  OTP.please try again later", notificationtype: TSMessageNotificationType.Error)
                }
        }
    }
    
    func parseResponse(response:SuccessResponse)
    {
        LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
        showNotificationMessageWithTypeOfNotifcation(EmailSubmitMessage, notificationtype:.Success)
    }
    
    func parseCheckResetTokenResponse(response : ProfileDetailResponse )
    {
        LoadingIndicatorManager.sharedInstance.hideLoadingIndicator()
        // API to update Temporary Password.
        guard let _ = response.result else
        {
            showNotificationMessageWithTypeOfNotifcation("Sorry, Couldn't process your request . please try again later", notificationtype: TSMessageNotificationType.Error)
            return
        }
        
       let userdetail = response.result?.first
        if let t_userdetail = userdetail {
            self.gotoPasswordResetPage(t_userdetail)
        }
        else{
            showNotificationMessageWithTypeOfNotifcation("Sorry, Couldn't process your request . please try again later", notificationtype: TSMessageNotificationType.Error)
        }
    }
}
