//
//  NewPasswordViewController.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 07/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

import UIKit

let internalServerError = "Internal server error"

class NewPasswordViewController: TRKRBaseViewController {
    
    @IBOutlet var newPasswordTextField: UITextField!
    @IBOutlet var newEmailView: UIView!
    var userDetails : UserProfileDetails?
    var forgotPasswordAPIManager = ForgotPasswordManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        TRKRUtility.setBGColor(self.view)
        if let navigationController = self.navigationController
        {
            navigationController.navigationBarHidden = false
        }
        setNavigationControllerWithBack(title: NavigationBarTitle.NewPassword)
        TRKRUtility.setBorder(newEmailView)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func setUserDetail(userdetail : UserProfileDetails) {
        userDetails = userdetail
    }
    
    
    @IBAction func newPasswordSubmitAction(sender: UIButton) {
        
        self.view.endEditing(true)
        let  pwd = newPasswordTextField.text
        
        if pwd?.characters.count > 0
        {
            showNotificationMessageWithTypeOfNotifcation("Password Updated successfully.", notificationtype: .Success)
            //gotoLoginPage()
            self.updateUserPassword(pwd!)
        }
        else
        {
            // enter password message
            showNotificationMessageWithTypeOfNotifcation("Please enter new password.", notificationtype: .Error)
        }
        
        
    }
    
    
    //MARK: New Password API
    func updateUserPassword(newPassword:String)
    {
        guard let _ = userDetails else {
            showNotificationMessageWithTypeOfNotifcation(internalServerError, notificationtype: .Error)
            return
        }
        
        if let userId = userDetails?.user_id
        {
            forgotPasswordAPIManager.updateUserPassword(userId, userPassword: newPassword, callback: { (response) -> () in
                    self.gotoLoginPage()
                }) { (error) -> () in
                    self.failureBlockHandler(error)
            }

        }
        
    }
    
    
    func gotoLoginPage() -> Void
    {
        let lVc = self.storyboard?.instantiateViewControllerWithIdentifier(TRKR_LOGIN_VC)
        if let navigationController = self.navigationController,loginVC = lVc
        {
            navigationController.popToRootViewControllerAnimated(false)
            navigationController.pushViewController(loginVC, animated: true)
        }
    }
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
