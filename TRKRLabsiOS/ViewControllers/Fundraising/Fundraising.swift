//
//  Fundraising.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 29/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import Device_swift

let fontColor : UIColor     = UIColor(colorLiteralRed: 197/255.0, green: 196/255.0, blue: 196/255.0, alpha: 1)
let fontFamiliy : String    = "Helvetica Neue"
let fontSize    : CGFloat   = 16

let contentString : String  = "We partnered with CrowdRise so you can help support elite athletes who cannot afford the TRKR app and sensors \n\n When you donate, please make sure you include in the comment box the team or athletic program you wish to support \n\n 100% of the money we receive through CrowdRise will go towards your choice."


class Fundraising: TRKRBaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var paragraphTextView : UITextView!
    @IBOutlet weak var quotes : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TRKRUtility.setBGColor(self.view)
        setNavigationController(title: NavigationBarTitle.Fundraising)
        
       
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    
    
    func configTitleLable(){
        // Setting Text for label and underline
        let attributedString : NSMutableAttributedString  = NSMutableAttributedString(string: "Support Your Favorite Elite Athletes")
        let rangeOfString : NSRange = NSMakeRange(0, attributedString.length)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(int: 1), range: rangeOfString )
        
     /*   // Setting Font Family and Size
        let fontForLabel : UIFont = UIFont(name:fontFamiliy, size: fontSize)!
        let attrsDictionary = NSDictionary(object: fontForLabel, forKey: NSFontAttributeName)
        attributedString.addAttributes(attrsDictionary as! [String : AnyObject], range: rangeOfString)
       */
        //Setting letter spacing
        let spacing = 1.5
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: rangeOfString)
        titleLabel.attributedText = attributedString
        titleLabel.textAlignment = .Center
        titleLabel.textColor = fontColor
    }
    
    
    func configParaLable(labelRef:UITextView,textInput:String){
        
        let attributedString : NSMutableAttributedString  = NSMutableAttributedString(string:textInput )
        let rangeOfString : NSRange = NSMakeRange(0, attributedString.length)
        let spacing = 1.5
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: rangeOfString)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: style, range: rangeOfString)
        
        
        let fontForLabel = UIFont.preferredFontForTextStyle(UIFontTextStyleFootnote)
        let attrsDictionary = NSDictionary(object: fontForLabel, forKey: NSFontAttributeName)
        attributedString.addAttributes(attrsDictionary as! [String : AnyObject], range: rangeOfString)

        labelRef.attributedText = attributedString
        labelRef.textAlignment = .Center
        labelRef.textColor = fontColor
    
        
    }
    
    func configUILable(labelRef:UILabel,textInput:String){
        
        let attributedString : NSMutableAttributedString  = NSMutableAttributedString(string:textInput )
        let rangeOfString : NSRange = NSMakeRange(0, attributedString.length)
        let spacing = 1.5
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: rangeOfString)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: style, range: rangeOfString)
        
        
        let fontForLabel = UIFont.preferredFontForTextStyle(getPreferredFontStyleForDevice())
        let attrsDictionary = NSDictionary(object: fontForLabel, forKey: NSFontAttributeName)
        attributedString.addAttributes(attrsDictionary as! [String : AnyObject], range: rangeOfString)
        
        labelRef.attributedText = attributedString
        labelRef.textAlignment = .Center
        labelRef.textColor = fontColor
        
        
    }
    
    func getPreferredFontStyleForDevice()->String
    {
        let deviceType = UIDevice.currentDevice().deviceType
        
        switch deviceType {
        case .IPhone6SPlus: return UIFontTextStyleSubheadline
        case .IPhone4 : return UIFontTextStyleFootnote
        default:return UIFontTextStyleBody
        }

    }
    
    @IBAction func submitButtonTouched(sender :AnyObject){
        
        UIApplication.sharedApplication().openURL(NSURL(string: FUNDRAISING_URL)!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
