//
//  AppDelegate.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 10/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var keyChainWrapper = KeychainWrapper()
    var mmDrawerController : MMDrawerController?
    let loginManager = LoginManager()
    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //this will enable disable Keyboard Options
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        //This will set the status bar white color in all screens.
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController:UIViewController
        let hasLoginKey = NSUserDefaults.standardUserDefaults().boolForKey(khasLoginKey)
        
        if hasLoginKey == false
        {
            //there is no login Key.Please use login
            initialViewController =  storyboard.instantiateViewControllerWithIdentifier(TRKR_FAN_ATHLETE_VC) as! FanOrAthleteModeViewController
            let navigationController:UINavigationController = UINavigationController(rootViewController: initialViewController)
            navigationController.navigationBarHidden = true
            self.window?.rootViewController = nil
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
        else
        {   // Go to Dashboard Page.
          //let email =  NSUserDefaults.standardUserDefaults().valueForKey(kUserEmail) as! String
          //let password = (keyChainWrapper.myObjectForKey(kSecValueData) as! String)
          // self.refreshUserToken(email, password: password)
            let role = NSUserDefaults.standardUserDefaults().valueForKey(kUserRole) as! String
            let  initialViewController:UIViewController = self.getmmDrawerControllerObject(role)
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        SDImageCache.sharedImageCache().cleanDisk()
        SDImageCache.sharedImageCache().clearMemory()
        
    }

    
    func getmmDrawerControllerObject(userRole:String) -> MMDrawerController{
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let centerViewController:UIViewController
        if userRole == "1"
        {
            centerViewController = mainStoryboard.instantiateViewControllerWithIdentifier(TRKR_DASHBOARD_VC) as! DashboardViewController
        }
        else
        {
            centerViewController = mainStoryboard.instantiateViewControllerWithIdentifier(TRKR_FANS_LIST_VC) as! FansListViewController
        }
        let leftViewController = mainStoryboard.instantiateViewControllerWithIdentifier(TRKR_MENU_VC)as! MenuViewController
        let centerNavigationController = UINavigationController(rootViewController: centerViewController)
        mmDrawerController = MMDrawerController(centerViewController: centerNavigationController , leftDrawerViewController: leftViewController)
        mmDrawerController!.openDrawerGestureModeMask = .PanningCenterView
        mmDrawerController!.closeDrawerGestureModeMask = .PanningCenterView
        return mmDrawerController!
    }
    
    
    
    //MARK : Refresh user token
    func refreshUserToken(email:String,password:String)
    {
        loginManager.login(email, password: password, callback: { (response:UserProfileResponse) -> () in
               self.parseResponse(email,password: password,response:response)
            })
            { (error:ErrorResponse) -> () in
                self.failureResponse(error)
            }
    }
    
    
    func parseResponse(email:String,password:String,response:UserProfileResponse)
    {
        UserProfileManager.sharedInstance.saveUserProfile(email, password: password, userProfile: response.result!)
    }
    
    func failureResponse(error:ErrorResponse)
    {
        TRKRUtility.showAlertMessage(kAPP_TITLE, message: error.errorDescription!,viewController: (self.window?.rootViewController)!)
    }
}

