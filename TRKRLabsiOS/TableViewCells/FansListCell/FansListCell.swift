//
//  FansListCell.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 14/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

import SWTableViewCell


class FansListCell: SWTableViewCell {
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var fanNameLabel: UILabel!
    @IBOutlet var fanLocationLabel: UILabel!
    @IBOutlet var fanLocationIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
