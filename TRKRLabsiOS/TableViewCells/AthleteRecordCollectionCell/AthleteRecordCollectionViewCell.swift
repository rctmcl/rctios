//
//  AthleteRecordCollectionViewCell.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 15/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

class AthleteRecordCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var statsIconImageView: UIImageView!
    @IBOutlet var statsTitleLabel: UILabel!
    @IBOutlet var statsValueLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
