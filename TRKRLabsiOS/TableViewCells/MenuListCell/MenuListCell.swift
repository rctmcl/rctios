//
//  MenuListCell.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 14/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

class MenuListCell: UITableViewCell {
    
    
    @IBOutlet var menuIconImageView: UIImageView!
    @IBOutlet var menuTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
