//
//  FanFollowRequestCell.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 18/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

class FanFollowRequestCell: UITableViewCell {
    
    @IBOutlet var fanProfileImageView: UIImageView!
    @IBOutlet var fanNameLabel: UILabel!
    @IBOutlet var fanLocationLabel: UILabel!
    @IBOutlet var followAcceptButton: UIButton!
    @IBOutlet var followRejectButton: UIButton!
    @IBOutlet var fanLocationIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
