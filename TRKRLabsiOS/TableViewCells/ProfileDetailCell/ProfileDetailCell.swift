//
//  ProfileDetailCell.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 19/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import UIKit

class ProfileDetailCell: UITableViewCell {
    @IBOutlet var profiePropertiesLabel: UILabel!
    @IBOutlet var profileValueTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
