//
//  AthleteRequestCell.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 03/01/16.
//  Copyright © 2016 MobileCloudLabs. All rights reserved.
//

import UIKit

class AthleteRequestCell: UITableViewCell {
    @IBOutlet var fanProfileImageView: UIImageView!
    @IBOutlet var fanNameLabel: UILabel!
    @IBOutlet var fanLocationLabel: UILabel!
    @IBOutlet var requestButton: UIButton!
    @IBOutlet var fanLocationIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func configRequestButton()
    {
        //requestButton.layer.masksToBounds = true
        requestButton.layer.cornerRadius = 5.0
        requestButton.layer.borderColor = UIColor.whiteColor().CGColor
        requestButton.layer.borderWidth = 2.0
        
    }
    
}
