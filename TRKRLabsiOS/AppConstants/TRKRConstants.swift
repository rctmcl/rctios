//
//  TRKRConstants.swift
//  TRKRLabsiOS
//
//  Created by inexgen on 10/12/15.
//  Copyright © 2015 MobileCloudLabs. All rights reserved.
//

import Foundation


//MARK: App URL
let kLocalURL       =   "http://192.168.2.32/trkr/api/"
let kDevURL         =   "http://52.20.100.202/index.php"
let kProductionURL  =   "http://52.20.100.202/index.php"

let TRKR_BASE_URL   =   kProductionURL


// set this key to false to disable logging
let kLoggingEnabled = false

let kUserEmail   :String = "user_email"
let kUserPassword:String = "user_password"
let khasLoginKey:String = "hasLoginKey"
let kUserToken:String   = "user_token"
let kUserRole:String    = "user_role"
let kUserId:String      = "user_id"

let kUserProfile  = "user_profile"

let API_SUCCESS_STATUS_CODE  = 10
let STATE_LIST_KEY           = "STATES_LIST"
let TRKR_DEFAULT_FONT = "HelveticaNeue-Medium"

let FUNDRAISING_URL = "https://www.crowdrise.com/trkrlabs"

//MARK: Measurements

//computed property: add a file to store the objects array
var stateListFilePath: String {
    let manager = NSFileManager.defaultManager()
    let url = manager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first! as NSURL
    return url.URLByAppendingPathComponent("statesList").path!
}


let TRKR_WEIGHT_UNIT = "Lbs"
let TRKR_HEIGHT_UNIT = "In"
let TRKR_MINUTES_UNIT = "Minutes"
let TRKR_HEART_BEAT_UNIT = "Beats/Minute"




//MARK: App Title

let kAPP_TITLE = "TRKR Labs"





//MARK: UIViewControllers Storyboard Identifiers

let TRKR_FAN_ATHLETE_VC     = "FANORATHLETEMODEVC"
let TRKR_LOGIN_VC           = "LOGIN_VC"
let TRKR_STATS_VC           = "STATS_VC"
let TRKR_DASHBOARD_VC       = "DASHBOARD_VC"
let TRKR_MENU_VC            = "MENU_VC"
let TRKR_FANS_LIST_VC       = "FANS_LIST_VC"
let TRKR_SETTINGS_VC        = "SETTINGS_VC"
let TRKR_FAN_DASHBOARD      = "FAN_DASHBOARD_VC"
let TRKR_FUNDRAISING_VC     = "TRKR_FUNDRAISING_VC"
let TRKR_INVITEATHLETE_VC   = "TRKR_INVITEATHLETE_VC"
let TRKR_FORGOT_PASSWORD_VC = "FORGOT_PASSWORD_VC"
let TRKR_NEW_PASSWORD_VC    = "NEW_PASSWORD_VC"



let FanName  = "John Doe"
let FanLocation = "Miami Springs, FL"




//MARK: Tableview Cell Identifiers

let MENU_LIST_CELL_ID  = "MENU_LIST_CELL"
let FANS_LIST_CELL_ID  = "FANS_LIST_CELL"
let DONUT_COLLECTION_CELL_ID = "DONUT_COLLECTION_CELL"
let ATHLETE_RECORD_COLLECTION_CELL_ID = "ATHLETE_RECORD_COLLECTION_CELL"
let FAN_FOLLOW_REQUEST_CELL   = "FAN_FOLLOW_REQUEST_CELL"
let PROFILE_DETAIL_CELL       = "PROFILE_DETAIL_CELL"
let ATHLETE_REQUEST_CELL      = "ATHLETE_REQUEST_CELL"


//MARK: Background Color

let bgColor1 :UIColor = UIColor(colorLiteralRed: 5.0/255.0, green: 58.0/255.0, blue: 200.0/255.0, alpha: 1.0)
let bgColor2 :UIColor = UIColor(colorLiteralRed: 8.0/255.0, green: 75.0/255.0, blue: 200.0/255.0, alpha: 0.7)
let bgColor3:UIColor  = UIColor(red: 6.0/255.0, green: 63.0/255.0, blue: 125.0/255.0, alpha: 1.0)

let color1 = UIColor(red: 2.0/255.0, green: 58.0/255.0, blue: 100.0/255.0, alpha: 0.8)
let color2 = UIColor(red: 8.0/255.0, green: 85/255.0, blue: 200.0/255.0, alpha: 0.8)

/*
let bgColor1:UIColor  = UIColor(red: 8.0/255.0, green: 57.0/255.0, blue: 95.0/255.0, alpha: 1.0)
let bgColor2:UIColor  = UIColor(red: 10.0/255.0, green: 76.0/255.0, blue: 149.0/255.0, alpha: 1.0)
let bgColor3:UIColor  = UIColor(red: 6.0/255.0, green: 63.0/255.0, blue: 125.0/255.0, alpha: 1.0)
*/

let navigationBarColor:UIColor = UIColor(red: 8.0/255.0, green: 51.0/255.0, blue: 105.0/255.0, alpha: 1.0)


//MARK: Athlete Record Colors

let EFFORT_COLOR:UIColor        =  UIColor(red: 32.0/255.0, green: 208.0/255.0, blue: 8.0/255.0, alpha: 1.0)
let STAMINA_COLOR:UIColor       = UIColor(red: 32.0/255.0, green: 208.0/255.0, blue: 8.0/255.0, alpha: 1.0)
let SLEEP_QUALITY_COLOR:UIColor = UIColor(red: 55.0/255.0, green: 174.0/255.0, blue: 213.0/255.0, alpha: 1.0)
let DONUT_CENTER_COLOR:UIColor  = UIColor(red: 6.0/255.0, green: 63.0/255.0, blue: 137.0/255.0, alpha: 1.0) // Donut Center color
/*
    // Pass the above two UIColor to achieve GradientStyle
 [UIColor colorWithGradientStyle:UIGradientStyleRadial withFrame:self.view.frame andColors:@[color2,color1]];
*/






